INSERT INTO random_names (race, sex, name)
VALUES ('Halfling', 'Male', 'Adalhard Brown'),
       ('Halfling', 'Male', 'Aigulf Gawkroger'),
       ('Halfling', 'Male', 'Alberic Brandybuck'),
       ('Halfling', 'Male', 'Angilbert Grubb'),
       ('Halfling', 'Male', 'Ansegar Barrowes'),
       ('Halfling', 'Male', 'Ansegisel Butcher'),
       ('Halfling', 'Male', 'Ansovald Brandagamba'),
       ('Halfling', 'Male', 'Arculf Brandybuck'),
       ('Halfling', 'Male', 'Audoneus Underlake'),
       ('Halfling', 'Male', 'Audramnus Puddlefoot'),
       ('Halfling', 'Male', 'Badegisel Harfoot'),
       ('Halfling', 'Male', 'Badegisel Swiftfoot'),
       ('Halfling', 'Male', 'Berenger Wanderfoot'),
       ('Halfling', 'Male', 'Bero Brandywood'),
       ('Halfling', 'Male', 'Bilba Greenhand'),
       ('Halfling', 'Male', 'Bilbo Longhole'),
       ('Halfling', 'Male', 'Bruno Oldbuck'),
       ('Halfling', 'Male', 'Bungo Hogpen'),
       ('Halfling', 'Male', 'Cerdic Chubb-Baggins'),
       ('Halfling', 'Male', 'Cerdic Hopesinger'),
       ('Halfling', 'Male', 'Charibert Featherbottom'),
       ('Halfling', 'Male', 'Chlotar Sandyman'),
       ('Halfling', 'Male', 'Chrodegang Silentfoot'),
       ('Halfling', 'Male', 'Dagobert Chubb'),
       ('Halfling', 'Male', 'Dudon Brockhouse'),
       ('Halfling', 'Male', 'Dudon Clayhanger'),
       ('Halfling', 'Male', 'Durand Smallburrow'),
       ('Halfling', 'Male', 'Engelbert Cotton'),
       ('Halfling', 'Male', 'Erenfried Rumblebelly'),
       ('Halfling', 'Male', 'Evroul Thornburrow'),
       ('Halfling', 'Male', 'Evroult Hogpen'),
       ('Halfling', 'Male', 'Falco Puddifoot'),
       ('Halfling', 'Male', 'Faramir Oldbuck'),
       ('Halfling', 'Male', 'Flambard Burrows'),
       ('Halfling', 'Male', 'Folco Zaragamba'),
       ('Halfling', 'Male', 'Fridolin Undertree'),
       ('Halfling', 'Male', 'Fridugis Hornblower'),
       ('Halfling', 'Male', 'Gerold Finnagund'),
       ('Halfling', 'Male', 'Grossman Proudbody'),
       ('Halfling', 'Male', 'Hagen Boulderhill'),
       ('Halfling', 'Male', 'Hildebald Hornblower'),
       ('Halfling', 'Male', 'Hildebald Sackville'),
       ('Halfling', 'Male', 'Hildibrand Fairbairn'),
       ('Halfling', 'Male', 'Hildifons Brockhouse'),
       ('Halfling', 'Male', 'Hubert Gardner'),
       ('Halfling', 'Male', 'Huebald Underlake'),
       ('Halfling', 'Male', 'Hugo Baggins'),
       ('Halfling', 'Male', 'Imbert Hornwood'),
       ('Halfling', 'Male', 'Isengrim Whitfoot'),
       ('Halfling', 'Male', 'Jago Sackville-Baggins'),
       ('Halfling', 'Male', 'Jolly Greenhand'),
       ('Halfling', 'Male', 'Leger Hornblower'),
       ('Halfling', 'Male', 'Longo Took-Took'),
       ('Halfling', 'Male', 'Lull Baggins'),
       ('Halfling', 'Male', 'Magnus Dewfoot'),
       ('Halfling', 'Male', 'Mattalic Gaukrogers'),
       ('Halfling', 'Male', 'Maura Took'),
       ('Halfling', 'Male', 'Meginhard Rumblebelly'),
       ('Halfling', 'Male', 'Mungo Hlothran'),
       ('Halfling', 'Male', 'Nithard Mugwort'),
       ('Halfling', 'Male', 'Nithard Noakesburrow'),
       ('Halfling', 'Male', 'Nob Hornblower'),
       ('Halfling', 'Male', 'Odger Bramblethorn'),
       ('Halfling', 'Male', 'Odovacar Goodsong'),
       ('Halfling', 'Male', 'Pacatian Bilberry'),
       ('Halfling', 'Male', 'Pacatian Smallburrow'),
       ('Halfling', 'Male', 'Razo Sandheaver'),
       ('Halfling', 'Male', 'Reginard Goldworthy'),
       ('Halfling', 'Male', 'Reolus Clayhanger'),
       ('Halfling', 'Male', 'Ricfried Stumbletoe'),
       ('Halfling', 'Male', 'Richomeres Undertree'),
       ('Halfling', 'Male', 'Sadoc Mugwort'),
       ('Halfling', 'Male', 'Samo Sandheaver'),
       ('Halfling', 'Male', 'Saradoc Underhill'),
       ('Halfling', 'Male', 'Sigismond Stoor'),
       ('Halfling', 'Male', 'Suidger Lothran'),
       ('Halfling', 'Male', 'Thankmar Rumble'),
       ('Halfling', 'Male', 'Theobald Diggle'),
       ('Halfling', 'Male', 'Tobias Undertree'),
       ('Halfling', 'Male', 'Trahand Gammidge'),
       ('Halfling', 'Male', 'Vedast Harfoot'),
       ('Halfling', 'Male', 'Waiofar Whitbottom'),
       ('Halfling', 'Male', 'Wala Galpsi'),
       ('Halfling', 'Male', 'Waltgaud Burrows'),
       ('Halfling', 'Male', 'Willibrord Swiftfoot'),
       ('Halfling', 'Female', 'Adallind Stoor'),
       ('Halfling', 'Female', 'Adallinda Gluttonbelly'),
       ('Halfling', 'Female', 'Adda Hopesinger'),
       ('Halfling', 'Female', 'Alexandria Bottomhill'),
       ('Halfling', 'Female', 'Alexandria Whitbottom'),
       ('Halfling', 'Female', 'Alfrida Fairfoot'),
       ('Halfling', 'Female', 'Amanda Wanderfoot'),
       ('Halfling', 'Female', 'Arabella Brownlock'),
       ('Halfling', 'Female', 'Aubirge Finnagund'),
       ('Halfling', 'Female', 'Baldechildis Gaukrogers'),
       ('Halfling', 'Female', 'Begga Swiftfoot'),
       ('Halfling', 'Female', 'Belba Proudbody'),
       ('Halfling', 'Female', 'Bell Riverhopper'),
       ('Halfling', 'Female', 'Beretrude Bottomhill'),
       ('Halfling', 'Female', 'Bertha Littlefoot'),
       ('Halfling', 'Female', 'Berthegund Gamgee'),
       ('Halfling', 'Female', 'Bertrada Gammidge'),
       ('Halfling', 'Female', 'Camelia Bunce'),
       ('Halfling', 'Female', 'Camelia Galbassi'),
       ('Halfling', 'Female', 'Cara Chubb'),
       ('Halfling', 'Female', 'Cassandra Cotton'),
       ('Halfling', 'Female', 'Cheyenne Featherbottom'),
       ('Halfling', 'Female', 'Christina Bramblethorn'),
       ('Halfling', 'Female', 'Clothild Bunce'),
       ('Halfling', 'Female', 'Clotilde Hornwood'),
       ('Halfling', 'Female', 'Cunegund Hedgehopper'),
       ('Halfling', 'Female', 'Deirdre Finnagund'),
       ('Halfling', 'Female', 'Destiny Fairbairn'),
       ('Halfling', 'Female', 'Devin Butcher'),
       ('Halfling', 'Female', 'Devin Mugwort'),
       ('Halfling', 'Female', 'Duenna Bolger'),
       ('Halfling', 'Female', 'Ealswid Bolger'),
       ('Halfling', 'Female', 'Elanor Fallohide'),
       ('Halfling', 'Female', 'Ellinrat Swiftfoot'),
       ('Halfling', 'Female', 'Engelberga Baggins'),
       ('Halfling', 'Female', 'Erin Burrows'),
       ('Halfling', 'Female', 'Ermentrudis Chubb'),
       ('Halfling', 'Female', 'Faileuba Goodwort'),
       ('Halfling', 'Female', 'Fatima Hedgehopper'),
       ('Halfling', 'Female', 'Gilly Silentfoot'),
       ('Halfling', 'Female', 'Gisela Silverstring'),
       ('Halfling', 'Female', 'Gloriana Lightfoot'),
       ('Halfling', 'Female', 'Guntheuc Hairyfoot'),
       ('Halfling', 'Female', 'Heather Lightfoot'),
       ('Halfling', 'Female', 'Heather Sandyman'),
       ('Halfling', 'Female', 'Herleva Noakesburrow'),
       ('Halfling', 'Female', 'Hildegund Diggle'),
       ('Halfling', 'Female', 'Hiltrude Brandybuck'),
       ('Halfling', 'Female', 'Ingunde Sandyman'),
       ('Halfling', 'Female', 'Itta Proudbody'),
       ('Halfling', 'Female', 'Jessica Longbottom'),
       ('Halfling', 'Female', 'Keira Brockhouse'),
       ('Halfling', 'Female', 'Kunegund Gaukrogers'),
       ('Halfling', 'Female', 'Lindsey Took'),
       ('Halfling', 'Female', 'Liutgarde Leafwalker'),
       ('Halfling', 'Female', 'Lobelia Noakes'),
       ('Halfling', 'Female', 'Mackenzie Goodwort'),
       ('Halfling', 'Female', 'Madelgarda Whitbottom'),
       ('Halfling', 'Female', 'Malva Swiftfoot'),
       ('Halfling', 'Female', 'Marigold Bilberry'),
       ('Halfling', 'Female', 'Melissa Proudbottom'),
       ('Halfling', 'Female', 'Moira Goodwort'),
       ('Halfling', 'Female', 'Nora Greenhill'),
       ('Halfling', 'Female', 'Peony Baggins'),
       ('Halfling', 'Female', 'Peony Fairfoot'),
       ('Halfling', 'Female', 'Rachel Bolger'),
       ('Halfling', 'Female', 'Regina Brockhouse'),
       ('Halfling', 'Female', 'Regintrude Hopesinger'),
       ('Halfling', 'Female', 'Rotrud Stoor'),
       ('Halfling', 'Female', 'Rotrud Took-Took'),
       ('Halfling', 'Female', 'Rotrude Gaukrogers'),
       ('Halfling', 'Female', 'Rotrude Littlefoot'),
       ('Halfling', 'Female', 'Rowan Brown'),
       ('Halfling', 'Female', 'Rowan Underburrow'),
       ('Halfling', 'Female', 'Ruothild Tunnelly'),
       ('Halfling', 'Female', 'Rylee Hopesinger'),
       ('Halfling', 'Female', 'Tara Fleetfoot'),
       ('Halfling', 'Female', 'Tara Goodchild'),
       ('Halfling', 'Female', 'Tasha Burrowes'),
       ('Halfling', 'Female', 'Tatiana Headstrong'),
       ('Halfling', 'Female', 'Tatiana Proudfoot'),
       ('Halfling', 'Female', 'Ultrogotha Fairbairn'),
       ('Halfling', 'Female', 'Waldrada Noakesburrow');

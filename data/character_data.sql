--Data the not class or race specific.
INSERT INTO sex (sex)
VALUES ('Male'),
       ('Female'),
       ('Neutral');

INSERT INTO alignments(alignment, nickname, description, character_alignment)
VALUES ('Lawful Good', 'Crusader',
        'A lawful good character acts as a good person is expected or required to act.' ||
        '   She combines a commitment to oppose evil with the discipline to fight relentlessly.' ||
        '   She tells the truth, keeps her word, helps those in need, and speaks out against injustice.' ||
        '   A lawful good character hates to see the guilty go unpunished.' ||
        '   Alhandra, a paladin who fights evil without mercy and protects the innocent without hesitation, is lawful good.' ||
        '   Lawful good is the best alignment you can be because it combines honor and compassion.', TRUE),
       ('Neutral Good',
        'Benefactor',
        'A neutral good character does the best that a good person can do.' ||
        '  He is devoted to helping others.' ||
        '  He works with kings and magistrates but does not feel beholden to them.' ||
        '  Jozan, a cleric who helps others according to their needs, is neutral good.' ||
        '  Neutral good is the best alignment you can be because it means doing what is good without bias for or against order.',
        TRUE),
       ('Chaotic Good', 'Rebel',
        'A chaotic good character acts as his conscience directs him with little regard for what others expect of him.' ||
        '   He makes his own way, but he’s kind and benevolent.' ||
        '   He believes in goodness and right but has little use for laws and regulations.' ||
        '   He hates it when people try to intimidate others and tell them what to do.' ||
        '   He follows his own moral compass, which, although good, may not agree with that of society.' ||
        '   Soverliss, a ranger who waylays the evil baron’s tax collectors, is chaotic good.' ||
        '   Chaotic good is the best alignment you can be because it combines a good heart with a free spirit.', TRUE),
       ('Lawful Neutral', 'Judge',
        'A lawful neutral character acts as law, tradition, or a personal code directs her.' ||
        '  Order and organization are para- mount to her.' ||
        '  She may believe in personal order and live by a code or standard, or she may believe in order for all and favor a strong, organized government.' ||
        '  Ember, a monk who follows her discipline with- out being swayed either by the demands of those in need or by the temptations of evil, is lawful neutral.' ||
        '  Lawful neutral is the best alignment you can be because it means you are reliable and honorable without being a zealot.',
        TRUE),
       ('Neutral', 'Undecided',
        'A neutral character does what seems to be a good idea.' ||
        '  She doesn’t feel strongly one way or the other when it comes to good vs. evil or law vs. chaos.' ||
        '  Most neutral characters exhibit a lack of conviction or bias rather than a com- mitment to neutrality.' ||
        '  Such a character thinks of good as better than evil—after all, she would rather have good neighbors and rulers than evil ones.' ||
        '  Still, she’s not personally committed to upholding good in any abstract or universal way.' ||
        '  Mialee, a wizard who devotes herself to her art and is bored by the semantics of moral debate, is neutral.' ||
        '  Some neutral characters, on the other hand, commit themselves philosophically to neutrality.' ||
        '  They see good, evil, law, and chaos as prejudices and dangerous extremes.' ||
        '  They advocate the middle way of neutrality as the best, most balanced road in the long run.' ||
        '  Neutral is the best alignment you can be because it means you act naturally, without prejudice or compulsion.',
        TRUE),
       ('Chaotic Neutral', 'Free Spirit',
        'A chaotic neutral character follows his whims.' ||
        '  He is an individualist first and last.' ||
        '  He values his own liberty but doesn’t strive to protect others’ freedom.' ||
        '  He avoids authority, resents restrictions, and challenges traditions.' ||
        '  A chaotic neutral character does not intentionally disrupt organizations as part of a campaign of anarchy.' ||
        '  To do so, he would have to be motivated either by good (and a desire to liberate others) or evil (and a Devis desire to make those different from himself suffer).' ||
        '  A chaotic neutral character may be unpredictable, but his behavior is not totally random.' ||
        '  He is not as likely to jump off a bridge as to cross it.' ||
        '  Gimble, a bard who wanders the land living by his wits, is chaotic neutral.' ||
        '  Chaotic neutral is the best alignment you can be because it represents true freedom from both society’s restrictions and a do- gooder’s zeal.',
        TRUE),
       ('Lawful Evil', 'Dominator',
        'A lawful evil villain methodically takes what he wants within the limits of his code of conduct without regard for whom it hurts.' ||
        '  He cares about tradition, loyalty, and order but not about freedom, dignity, or life.' ||
        '  He plays by the rules but without mercy or compassion.' ||
        '  He is comfortable in a hierarchy and would like to rule, but is willing to serve.' ||
        '  He condemns others not according to their actions but according to race, religion, homeland, or social rank.' ||
        '  He is loath to break laws or promises.' ||
        '  This reluctance comes partly from his nature and partly because he depends on order to protect himself from those who oppose him on moral grounds.' ||
        '  Some lawful evil villains have particular taboos, such as not killing in cold blood (but having underlings do it) or not letting children come to harm (if it can be helped).' ||
        '  They imagine that these compunctions put them above unprincipled villains.' ||
        '  The scheming baron who expands his power and exploits his people is lawful evil.' ||
        '  Some lawful evil people and creatures commit themselves to evil with a zeal like that of a crusader committed to good.' ||
        '  Beyond being willing to hurt others for their own ends, they take pleasure in spreading evil as an end unto itself.' ||
        '  They may also see doing evil as part of a duty to an evil deity or master.' ||
        '  Lawful evil is sometimes called “diabolical,” because devils are the epitome of lawful evil.' ||
        '  Lawful evil is the most dangerous alignment because it represents methodical, intentional, and frequently successful evil.',
        FALSE),
       ('Neutral Evil', 'Malefactor',
        'A neutral evil villain does whatever she can get away with.' ||
        '  She is out for herself, pure and simple.' ||
        '  She sheds no tears for those she kills, whether for profit, sport, or convenience.' ||
        '  She has no love of order and holds no illusion that following laws, traditions, or codes would make her any better or more noble.' ||
        '  On the other hand, she doesn’t have the restless nature or love of conflict that a chaotic evil villain has.' ||
        '  The criminal who robs and murders to get what she wants is neutral evil.' ||
        '  Some neutral evil villains hold up evil as an ideal, committing evil for its own sake.' ||
        '  Most often, such villains are devoted to evil deities or secret societies.' ||
        '  Neutral evil is the most dangerous alignment because it represents pure evil without honor and without variation.',
        FALSE),
       ('Chaotic Evil', 'Destroyer',
        'A chaotic evil character does what- ever his greed, hatred, and lust for destruction drive him to do.' ||
        '  He is hot-tempered, vicious, arbitrarily violent, and unpredictable.' ||
        '  If he is simply out for whatever he can get, he is ruthless and brutal.' ||
        '  If he is committed to the spread of evil and chaos, he is even worse.' ||
        '  Thankfully, his plans are haphazard, and any groups he joins or forms are poorly organized.' ||
        '  Typically, chaotic evil people can be made to work together only by force, and their leader lasts only as long as he can thwart attempts to topple or assassinate him.' ||
        '  The demented sorcerer pursuing mad schemes of vengeance and havoc is chaotic evil.' ||
        '  Chaotic evil is sometimes called “demonic” because demons are the epitome of chaotic evil.' ||
        '  Chaotic evil is the most dangerous alignment because it represents the destruction not only of beauty and life but also of the order on which beauty and life depend.',
        FALSE);

-- Table 3-7
INSERT INTO deities(deity, description, alignment, typical_worshipers)
VALUES ('Heironeous', 'The God of Valor', 'Lawful Good', 'Paladins, Fighters, Monks'),
       ('Moradin', 'The God of the Dwarves', 'Lawful Good', 'Dwarves'),
       ('Yondalla', 'The Goddess of the Halflings', 'Lawful Good', 'Halflings'),
       ('Ehlonna', 'The Goddess of the Woodlands', 'Neutral Good',
        'Elves, Gnomes, Half-Elves, Halflings, Rangers, Druids'),
       ('Garl Glittergold', 'The God of the Gnomes', 'Neutral Good', 'Gnomes'),
       ('Pelor', 'The God of the Sun', 'Neutral Good', 'Ranger, Bards'),
       ('Corellon Larethian', 'The God of the Elves', 'Chaotic Good', 'Elves, Half-Elves, Bards'),
       ('Kord', 'The God of Strength', 'Chaotic Good', 'Fighters, Barbarians, Rogues, athletes'),
       ('Wee Jas', 'The Goddess of Death and Magic', 'Lawful Neutral', 'Wizards, Necromancers, Sorcerers'),
       ('St. Cuthbert', 'The God of Retribution', 'Lawful Neutral', 'Fighters, Monks, Soldiers'),
       ('Fharlanghn', 'The God of Roads', 'Neutral', 'Bards, Adventurers, Merchants'),
       ('Boccob', 'The God of Magic', 'Neutral', 'Wizards, sorcerers, sages'),
       ('Obad-Hai', 'The God of Nature', 'Neutral', 'Druids, Barbarians, Rangers'),
       ('Olidammara', 'The God of Thieves', 'Chaotic Neutral', 'Rogues, Bards, Thieves'),
       ('Hextor', 'The God of Tyranny', 'Lawful Evil', 'Evil Fighters, Monks'),
       ('Nerull', 'The God of Death', 'Neutral Evil', 'Evil Necromancers, Rogues'),
       ('Vecna', 'The God of Secrets', 'Neutral Evil', 'Evil Wizards, Sorcerers, Rogues, Spies'),
       ('Erythnul', 'The God of Slaughter', 'Neutral Evil', 'Evil Fighters, Barbarians, Rogues'),
       ('Gruumsh', 'The God of the Orcs', 'Chaotic Evil', 'Half-Orcs, Orcs');

INSERT INTO domains(domain)
VALUES ('Air'),
       ('Animal'),
       ('Chaos'),
       ('Death'),
       ('Destruction'),
       ('Earth'),
       ('Evil'),
       ('Fire'),
       ('Good'),
       ('Healing'),
       ('Knowledge'),
       ('Law'),
       ('Luck'),
       ('Magic'),
       ('Plant'),
       ('Protection'),
       ('Strength'),
       ('Sun'),
       ('Travel'),
       ('Trickery'),
       ('War'),
       ('Water');

-- Table 3-7
INSERT INTO domains_deity(domain, deity)
VALUES ('Air', 'Obad-Hai'),
       ('Animal', 'Ehlonna'),
       ('Animal', 'Obad-Hai'),
       ('Chaos', 'Corellon Larethian'),
       ('Chaos', 'Gruumsh'),
       ('Chaos', 'Kord'),
       ('Chaos', 'Olidammara'),
       ('Death', 'Nerull'),
       ('Death', 'Wee Jas'),
       ('Destruction', 'St. Cuthbert'),
       ('Destruction', 'Hextor'),
       ('Earth', 'Moradin'),
       ('Earth', 'Obad-Hai'),
       ('Evil', 'Erythnul'),
       ('Evil', 'Gruumsh'),
       ('Evil', 'Hextor'),
       ('Evil', 'Nerull'),
       ('Evil', 'Vecna'),
       ('Fire', 'Obad-Hai'),
       ('Good', 'Corellon Larethian'),
       ('Good', 'Ehlonna'),
       ('Good', 'Garl Glittergold'),
       ('Good', 'Heironeous'),
       ('Good', 'Kord'),
       ('Good', 'Moradin'),
       ('Good', 'Pelor'),
       ('Good', 'Yondalla'),
       ('Knowledge', 'Boccob'),
       ('Knowledge', 'Vecna'),
       ('Law', 'Heironeous'),
       ('Law', 'Hextor'),
       ('Law', 'Moradin'),
       ('Law', 'St. Cuthbert'),
       ('Law', 'Wee Jas'),
       ('Law', 'Yondalla'),
       ('Luck', 'Fharlanghn'),
       ('Luck', 'Kord'),
       ('Luck', 'Olidammara'),
       ('Magic', 'Boccob'),
       ('Magic', 'Vecna'),
       ('Magic', 'Wee Jas'),
       ('Plant', 'Ehlonna'),
       ('Plant', 'Obad-Hai'),
       ('Protection', 'Corellon Larethian'),
       ('Protection', 'Fharlanghn'),
       ('Protection', 'Garl Glittergold'),
       ('Protection', 'Moradin'),
       ('Protection', 'St. Cuthbert'),
       ('Protection', 'Yondalla'),
       ('Strength', 'Gruumsh'),
       ('Strength', 'Kord'),
       ('Strength', 'Pelor'),
       ('Strength', 'St. Cuthbert'),
       ('Sun', 'Ehlonna'),
       ('Sun', 'Pelor'),
       ('Travel', 'Fharlanghn'),
       ('Trickery', 'Boccob'),
       ('Trickery', 'Erythnul'),
       ('Trickery', 'Garl Glittergold'),
       ('Trickery', 'Nerull'),
       ('Trickery', 'Olidammara'),
       ('War', 'Corellon Larethian'),
       ('War', 'Erythnul'),
       ('War', 'Gruumsh'),
       ('War', 'Heironeous'),
       ('War', 'Hextor');

INSERT INTO abilities(ability, ability_abbr, ability_description)
VALUES ('Strength', 'str',
        'Strength measures your character’s muscle and physical power.' ||
        '  This ability is especially important for fighters, barbarians, paladins, rangers, and monks because it helps
        them prevail in combat.' ||
        ' Strength also limits the amount of equipment you character can carry.'),
       ('Dexterity', 'dex',
        'Dexterity measures hand - eye coordination, agility, reflexes, and balance.' ||
        ' This ability is the most important ability for rogues,
        but it’s also high on the list for characters who typically wear light or medium armor (rangers and barbarians) or no armor at all (monks, wizards, and sorcerers), and
        for anyone who wants to be a skilled archer.'),
       ('Constitution', 'con',
        'Constitution represents your character’s health and stamina.' ||
        ' A Constitution bonus increases a character’s hit points, so the ability is important for all classes.'),
       ('Intelligence', 'itl',
        'Intelligence determines how well your character learns and reasons.' ||
        '  This ability is important for wizards because it affects how many spells they can cast,
        how hard their spells are to resist, and how powerful their spells can be.' ||
        ' It’s also important for any character who wants to have a wide assortment of skills.'),
       ('Wisdom', 'wis',
        'Wisdom describes a character’s willpower, common sense, perception, and intuition.' ||
        '  While Intelligence represents one’s ability to analyze information, Wisdom represents being in tune with and aware of one’s surroundings.' ||
        '  An "absentminded professor" has low Wisdom and high Intelligence.' ||
        '  A simpleton (low Intelligence) might still have great insight (high Wisdom).' ||
        '  Wisdom is the most important ability for clerics and druids, and it is also important for paladins and
        rangers.' ||
        '  If you want your character to have acute senses, put a high score in Wisdom.' ||
        '  Every creature has a Wisdom score.'),
       ('Charisma', 'chr',
        'Charisma measures a character’s force of personality, persuasiveness, personal magnetism, ability to lead, and
        physical attractiveness.' ||
        '  This ability represents actual strength of personality, not merely how one is perceived by others in a social setting.' ||
        '  Charisma is most important for paladins, sorcerers, and bards.' ||
        '  It is also important for clerics, since it affects their ability to turn undead.' ||
        '  Every creature has a Charisma score.');

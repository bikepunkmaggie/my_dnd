-- Hard coded, seemed best.
INSERT INTO deity_race(race, deity)
VALUES ('Dwarf', 'Moradin'),
       ('Elf', 'Corellon Larethian'),
       ('Elf', 'Ehlonna'),
       ('Gnome', 'Garl Glittergold'),
       ('Half-Elf', 'Corellon Larethian'),
       ('Half-Elf', 'Ehlonna'),
       ('Half-Orc', 'Gruumsh'),
       ('Halfling', 'Ehlonna'),
       ('Halfling', 'Yondalla');

-- Allowed cleric.  First non-race dependent deity.
INSERT INTO allowed_cleric_deity (race, deity)
SELECT races.race, deities.deity
FROM deities
         CROSS JOIN races
WHERE deities.deity NOT IN (SELECT deity FROM deity_race);

-- Race dependent.
INSERT INTO allowed_cleric_deity (race, deity)
SELECT race, deity
FROM deity_race;

-- Allowed alignments

---Barbarians are never lawful.
INSERT INTO allowed_alignments
SELECT 'Barbarian', alignment
FROM alignments
WHERE alignment NOT LIKE '%Lawful%'
  AND character_alignment = TRUE;

-- The spontaneous talent, magic, and lifestyle of the bard are incompatible with a lawful alignment.
INSERT INTO allowed_alignments
SELECT 'Bard', alignment
FROM alignments
WHERE alignment NOT LIKE '%Lawful'
  AND character_alignment = TRUE;

INSERT INTO allowed_alignments
SELECT 'Cleric', alignment
FROM alignments
WHERE character_alignment = TRUE;

--As such, they must be neutral on at least one alignment axis (chaotic–lawfulor good–evil), if not both.
INSERT INTO allowed_alignments
SELECT 'Druid', alignment
FROM alignments
WHERE alignment LIKE '%Neutral%'
  AND character_alignment = TRUE;

INSERT INTO allowed_alignments
SELECT 'Fighter', alignment
FROM alignments
WHERE character_alignment = TRUE;

-- Only those who are lawful at heart are capable of undertaking it.
INSERT INTO allowed_alignments
SELECT 'Monk', alignment
FROM alignments
WHERE alignment LIKE '%Lawful%'
  AND character_alignment = TRUE;

--Paladins must be lawful good
INSERT INTO allowed_alignments
SELECT 'Paladin', alignment
FROM alignments
WHERE alignment IS 'Lawful Good'
  AND character_alignment = TRUE;

INSERT INTO allowed_alignments
SELECT 'Ranger', alignment
FROM alignments
WHERE character_alignment = TRUE;

INSERT INTO allowed_alignments
SELECT 'Rogue', alignment
FROM alignments
WHERE character_alignment = TRUE;

INSERT INTO allowed_alignments
SELECT 'Sorcerer', alignment
FROM alignments
WHERE character_alignment = TRUE;

INSERT INTO allowed_alignments
SELECT 'Wizard', alignment
FROM alignments
WHERE character_alignment = TRUE;

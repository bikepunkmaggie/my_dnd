INSERT INTO size_information(size, attack_modifier, grapple_hide_modifier, dimension_min, dimension_max, weight_min,
                             weight_max, space, reach_tall, reach_long)
VALUES ('Fine', 8, -16, 0, 0.5, 0, 0.125, 0.5, 0, 0),
       ('Diminutive', 4, -12, 0.5, 1, 0.125, 1, 1, 0, 0),
       ('Tiny', 2, -8, 1, 2, 1, 8, 2.5, 0, 0),
       ('Small', 1, -4, 2, 4, 8, 60, 5, 5, 0),
       ('Medium', 0, 0, 4, 8, 60, 500, 5, 5, 5),
       ('Large', -1, 4, 8, 16, 500, 4000, 10, 10, 5),
       ('Huge', -2, 8, 16, 32, 4000, 32000, 15, 15, 10),
       ('Gargantuan', -4, 12, 32, 64, 32000, 250000, 20, 20, 15),
       ('Colossal', -8, 16, 64, 9999999, 250000, 999999, 30, 30, 20);

INSERT INTO monster_types(monster_type, type_description)
VALUES ('Aberration',
        'An aberration has a bizarre anatomy, strange abilities, an alien mindset, or any combination of the three.'),
       ('Air',
        'This subtype usually is used for elementals and outsiders with a connection to the Elemental Plane Air.' ||
        'Air creatures always have fly speeds and usually have perfect maneuverability.'),
       ('Angel',
        'Angels are a race of celestials, or good outsiders, native to the good-aligned Outer Planes.'),
       ('Animal',
        'An animal is a living, nonhuman creature, usually a vertebrate with no magical abilities and no innate capacity for language or culture.'),
       ('Aquatic', 'These creatures always have swim speeds and thus can move in water without making Swim checks.' ||
                   'An aquatic creature can breathe underwater.' ||
                   'It cannot also breathe air unless it has the amphibious special quality.'),
       ('Archon',
        'Archons are a race of celestials, or good outsiders, native to the Seven Mounting Heavens of Celestia.'),
       ('Augmented', 'A creature receives this subtype whenever something happens to change its original type.'),
       ('Goblinoid', 'Goblinoids are stealthy humanoids who live by hunting and raiding and who all speak Goblin.'),
       ('Water',
        'This subtype usually is used for elementals and outsiders with a connection to the Elemental Plane of Water.' ||
        'Creatures with the water subtype always have swim speeds and can move in water without making Swim checks.' ||
        'A water creature can breathe underwater and usually can breathe air as well.');


INSERT INTO monsters(name, size, sub_type, hit_die_rolls, hit_die_sides, hit_die_bonus_points, initiative, speed,
                     armor_class, base_attack, grapple, attack)
VALUES ('Aboleth', 'Huge', 'Aberration', 8, 8, 40, 1, 10, 16, 6, 22, 12),
       ('Goblin', 'Small', 'Goblinoid', 1, 8, 1, 1, 30, 15, 1, -3, 2),
       ('Hobgoblin', 'Medium', 'Goblinoid', 1, 8, 2, 1, 30, 15, 1, 2, 2);

INSERT INTO monster_movements (name, burrow, climb, fly, swim)
VALUES ('Aboleth', 0, 0, 0, 60);

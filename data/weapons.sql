INSERT INTO weapon_proficiency (proficiency)
VALUES ('Simple'),
       ('Exotic'),
       ('Martial');

INSERT INTO weapon_combat_type(combat_type)
VALUES ('Melee'),
       ('Ranged');

INSERT INTO weapon_encumbrance (encumbrance)
VALUES ('Light'),
       ('One-Handed'),
       ('Two-Handed');

INSERT INTO weapon_types (type)
VALUES ('Piercing'),
       ('Slashing'),
       ('Bludgeoning'),
       ('None');
INSERT INTO weapons (weapon, proficiency, combat_type, encumbrance, type, type_or, type_and, cost,
                     damage_small_die_rolls,
                     damage_small_die_sides, damage_medium_die_rolls,
                     damage_medium_die_sides, double_damage_small_die_rolls, double_damage_small_die_sides,
                     double_damage_medium_die_rolls, double_damage_medium_die_sides, critical, threat, range_increment,
                     weight)
VALUES ('Dagger', 'Simple', 'Melee', 'Light',
        'Piercing', 'Slashing', 'None', 2, 1, 2,
        1, 4, NULL, NULL, NULL, NULL,
        2, 19, 10, 1),
       ('Javelin', 'Simple', 'Ranged',
        'One-Handed', 'Piercing', 'None', 'None', 1,
        1, 4, 1, 6,
        NULL, NULL, NULL,
        NULL,
        2, NULL, 30, 2),
       ('Longsword', 'Martial', 'Melee',
        'One-Handed', 'Slashing', 'None', 'None', 15,
        1, 6, 1, 8,
        NULL, NULL, NULL,
        NULL,
        2, 19, NULL, 4),
       ('Morningstar', 'Simple', 'Melee',
        'One-Handed', 'Bludgeoning', 'None', 'Piercing', 8,
        1, 6, 1, 8,
        NULL, NULL, NULL,
        NULL,
        2, NULL, NULL, 6);

INSERT INTO weapon_descriptions (weapon, description)
VALUES ('Dagger',
        'The dagger is a common secondary weapon. You get a +2 bonus on Sleight of Hand checks made to conceal a dagger on your body.');


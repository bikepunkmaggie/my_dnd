PRAGMA foreign_keys = ON;
VACUUM;

--Set up primary tables.  Order by chapter.
--Current to chapter 6, religion
CREATE TABLE races
(
    race TEXT NOT NULL PRIMARY KEY
) WITHOUT ROWID;

CREATE TABLE abilities
(
    ability             TEXT NOT NULL PRIMARY KEY,
    ability_abbr        TEXT NOT NULL UNIQUE,
    ability_description TEXT NOT NULL
) WITHOUT ROWID;


-- Table on page 82
CREATE TABLE languages
(
    language TEXT    NOT NULL PRIMARY KEY,
    speakers TEXT    NOT NULL,
    alphabet INTEGER NOT NULL,
    FOREIGN KEY (alphabet) REFERENCES alphabets (alphabet)
) WITHOUT ROWID;


CREATE TABLE alphabets
(
    alphabet TEXT NOT NULL PRIMARY KEY
) WITHOUT ROWID;


CREATE TABLE classes
(
    char_class TEXT NOT NULL PRIMARY KEY
) WITHOUT ROWID;


CREATE TABLE skills
(
    skill_id            INTEGER NOT NULL CHECK (skill_id < 44) PRIMARY KEY,
    skill_name          TEXT    NOT NULL UNIQUE,
    key_ability         INTEGER REFERENCES abilities (ability_abbr),
    description         TEXT,
    armor_check_penalty BOOLEAN,
    can_be_learned      BOOLEAN,
    check_index         INTEGER,
    action              INTEGER,
    try_again           BOOLEAN,
    special_code        INTEGER,
    synergy_index       INTEGER,
    untrained_index     INTEGER
);

CREATE TABLE feats
(
    feat_name    TEXT NOT NULL PRIMARY KEY,
    description  TEXT NOT NULL,
    prerequisite TEXT NOT NULL,
    benefit      TEXT NOT NULL,
    normal       TEXT,
    special      TEXT
) WITHOUT ROWID;


CREATE TABLE alignments
(
    alignment           TEXT NOT NULL PRIMARY KEY,
    nickname            TEXT NOT NULL,
    description         TEXT NOT NULL,
    character_alignment BOOLEAN
) WITHOUT ROWID;


CREATE TABLE allowed_alignments
(
    char_class TEXT NOT NULL,
    alignment  TEXT NOT NULL,
    PRIMARY KEY (char_class, alignment),
    FOREIGN KEY (char_class) REFERENCES classes (char_class),
    FOREIGN KEY (alignment) REFERENCES alignments (alignment)
) WITHOUT ROWID;



CREATE TABLE deities
(
    deity              TEXT NOT NULL PRIMARY KEY,
    description        TEXT NOT NULL,
    alignment          TEXT NOT NULL,
    typical_worshipers TEXT NOT NULL,
    FOREIGN KEY (alignment) REFERENCES alignments (alignment)
) WITHOUT ROWID;


CREATE TABLE domains
(
    domain TEXT NOT NULL PRIMARY KEY
) WITHOUT ROWID;


CREATE TABLE deity_race
(
    race  TEXT NOT NULL,
    deity TEXT NOT NULL,
    PRIMARY KEY (race, deity),
    FOREIGN KEY (race) REFERENCES races (race),
    FOREIGN KEY (deity) REFERENCES deities (deity)
) WITHOUT ROWID;


CREATE TABLE domains_deity
(
    domain TEXT NOT NULL,
    deity  TEXT NOT NULL,
    PRIMARY KEY (domain, deity),
    FOREIGN KEY (deity) REFERENCES deities (deity),
    FOREIGN KEY (domain) REFERENCES domains (domain)
) WITHOUT ROWID;


CREATE TABLE allowed_cleric_deity
(
    deity TEXT NOT NULL,
    race  TEXT NOT NULL,
    PRIMARY KEY (deity, race),
    FOREIGN KEY (deity) REFERENCES deities (deity),
    FOREIGN KEY (race) REFERENCES races (race)
) WITHOUT ROWID;


CREATE TABLE sex
(
    sex TEXT NOT NULL PRIMARY KEY
) WITHOUT ROWID;


-- Character names: thanks to https://www.fantasynamegenerators.com/
CREATE TABLE random_names
(
    race TEXT NOT NULL,
    sex  TEXT NOT NULL,
    name TEXT PRIMARY KEY,
    FOREIGN KEY (race) REFERENCES races (race),
    FOREIGN KEY (sex) REFERENCES sex (sex)
) WITHOUT ROWID;


--Set up description tables
CREATE TABLE race_descriptions
(
    race        TEXT NOT NULL PRIMARY KEY,
    general     TEXT NOT NULL,
    personality TEXT NOT NULL,
    physical    TEXT NOT NULL,
    relations   TEXT NOT NULL,
    alignment   TEXT NOT NULL,
    lands       TEXT NOT NULL,
    religion    TEXT NOT NULL,
    language    TEXT NOT NULL,
    names       TEXT NOT NULL,
    adventures  TEXT NOT NULL,
    FOREIGN KEY (race) REFERENCES races (race)
) WITHOUT ROWID;


CREATE TABLE base_race_traits
(
    race       TEXT    NOT NULL PRIMARY KEY,
    size       TEXT    NOT NULL,
    extra_feat BOOLEAN NOT NULL,
    land_speed INTEGER NOT NULL,
    str_adj    INTEGER DEFAULT 0,
    dex_adj    INTEGER DEFAULT 0,
    con_adj    INTEGER DEFAULT 0,
    itl_adj    INTEGER DEFAULT 0,
    wis_adj    INTEGER DEFAULT 0,
    chr_adj    INTEGER DEFAULT 0,
    FOREIGN KEY (race) REFERENCES races (race)
) WITHOUT ROWID;


CREATE TABLE starting_age
(
    race       TEXT    NOT NULL,
    char_class TEXT    NOT NULL,
    adulthood  INTEGER NOT NULL,
    rolls      INTEGER NOT NULL,
    die        INTEGER NOT NULL,
    PRIMARY KEY (race, char_class),
    FOREIGN KEY (race) REFERENCES races (race),
    FOREIGN KEY (char_class) REFERENCES classes (char_class)
) WITHOUT ROWID;


CREATE TABLE starting_height_weight
(
    race         TEXT    NOT NULL,
    sex          TEXT    NOT NULL,
    base_height  INTEGER NULL,     -- In Inches
    height_rolls INTEGER NOT NULL,
    height_die   INTEGER NOT NULL,
    base_weight  INTEGER NOT NULL, -- In lbs
    weight_rolls INTEGER NOT NULL,
    weight_die   INTEGER NOT NULL,
    PRIMARY KEY (race, sex),
    FOREIGN KEY (race) REFERENCES races (race),
    FOREIGN KEY (sex) REFERENCES sex (sex)
) WITHOUT ROWID;


CREATE TABLE base_class_traits
(
    char_class       TEXT    NOT NULL PRIMARY KEY,
    hit_die          INTEGER NOT NULL,
    skill_per_level  INTEGER NOT NULL,
    start_gold_rolls INTEGER NOT NULL,
    FOREIGN KEY (char_class) REFERENCES classes (char_class)
) WITHOUT ROWID;

CREATE TABLE class_descriptions
(
    char_class      TEXT NOT NULL PRIMARY KEY,
    general         TEXT NOT NULL,
    adventures      TEXT NOT NULL,
    characteristics TEXT NOT NULL,
    alignment       TEXT NOT NULL,
    religion        TEXT NOT NULL,
    background      TEXT NOT NULL,
    races           TEXT NOT NULL,
    other_classes   TEXT NOT NULL,
    role            TEXT NOT NULL,
    FOREIGN KEY (char_class) REFERENCES classes (char_class)
) WITHOUT ROWID;


--Cross-reference tables
CREATE TABLE class_skills
(
    char_class        TEXT    NOT NULL PRIMARY KEY,
    skill_id          INTEGER NOT NULL REFERENCES skills (skill_id),
    class_skill       BOOLEAN,
    cross_class_skill BOOLEAN,
    FOREIGN KEY (char_class) REFERENCES classes (char_class)
) WITHOUT ROWID;

-- Weapons are from Chapter 7 and table 7-5
CREATE TABLE weapon_proficiency
(
    proficiency TEXT NOT NULL PRIMARY KEY
) WITHOUT ROWID;

CREATE TABLE weapon_combat_type
(
    combat_type TEXT NOT NULL PRIMARY KEY
) WITHOUT ROWID;

CREATE TABLE weapon_encumbrance
(
    encumbrance TEXT NOT NULL PRIMARY KEY
) WITHOUT ROWID;

CREATE TABLE weapon_types
(
    type TEXT PRIMARY KEY
) WITHOUT ROWID;

CREATE TABLE weapons
(
    weapon                         TEXT    NOT NULL PRIMARY KEY,
    proficiency                    TEXT    NOT NULL,
    combat_type                    TEXT    NOT NULL,
    encumbrance                    TEXT    NOT NULL,
    type                           TEXT    NOT NULL,
    type_or                        TEXT    NOT NULL,
    type_and                       TEXT    NOT NULL,
    cost                           INTEGER NOT NULL,
    damage_small_die_rolls         INTEGER NOT NULL,
    damage_small_die_sides         INTEGER NOT NULL,
    damage_medium_die_rolls        INTEGER NOT NULL,
    damage_medium_die_sides        INTEGER NOT NULL,
    double_damage_small_die_rolls  INTEGER,
    double_damage_small_die_sides  INTEGER,
    double_damage_medium_die_rolls INTEGER,
    double_damage_medium_die_sides INTEGER,
    critical                       INTEGER,
    threat                         INTEGER,
    range_increment                INTEGER,
    weight                         INTEGER,
    FOREIGN KEY (proficiency) REFERENCES weapon_proficiency (proficiency),
    FOREIGN KEY (combat_type) REFERENCES weapon_combat_type (combat_type),
    FOREIGN KEY (encumbrance) REFERENCES weapon_encumbrance (encumbrance),
    FOREIGN KEY (type) REFERENCES weapon_types (type),
    FOREIGN KEY (type_or) REFERENCES weapon_types (type),
    FOREIGN KEY (type_and) REFERENCES weapon_types (type)

);


CREATE TABLE weapon_descriptions
(
    weapon TEXT NOT NULL PRIMARY KEY,
    description NOT NULL,
    FOREIGN KEY (weapon) REFERENCES weapons (weapon)
) WITHOUT ROWID;


--Character
CREATE TABLE characters
(
    uuid          TEXT    NOT NULL PRIMARY KEY,
    name          TEXT    NOT NULL,
    race          TEXT    NOT NULL,
    char_class    TEXT    NOT NULL,
    level         INTEGER NOT NULL,
    xp            INTEGER NOT NULL,
    strength      INTEGER NOT NULL,
    dexterity     INTEGER NOT NULL,
    constitution  INTEGER NOT NULL,
    intelligence  INTEGER NOT NULL,
    wisdom        INTEGER NOT NULL,
    charisma      INTEGER NOT NULL,
    alignment     TEXT    NOT NULL,
    deity         TEXT,
    sex           TEXT    NOT NULL,
    age           INTEGER NOT NULL,
    height        INTEGER NOT NULL,
    weight        INTEGER NOT NULL,
    eye_color     TEXT    NOT NULL,
    skin_color    TEXT    NOT NULL,
    new_character BOOLEAN DEFAULT false,
    FOREIGN KEY (race) REFERENCES races (race),
    FOREIGN KEY (char_class) REFERENCES classes (char_class)
) WITHOUT ROWID;

--New character
CREATE TABLE new_characters
(
    uuid      TEXT    NOT NULL PRIMARY KEY,
    roll_1    INTEGER NOT NULL,
    roll_2    INTEGER NOT NULL,
    roll_3    INTEGER NOT NULL,
    roll_4    INTEGER NOT NULL,
    roll_5    INTEGER NOT NULL,
    roll_6    INTEGER NOT NULL,
    str_index INTEGER,
    dex_index INTEGER,
    con_index INTEGER,
    int_index INTEGER,
    wis_index INTEGER,
    cha_index INTEGER,
    FOREIGN KEY (uuid) REFERENCES characters (uuid)
) WITHOUT ROWID;


--Monsters
-- Table 7-1
CREATE TABLE size_information
(
    size                  TEXT    NOT NULL PRIMARY KEY,
    attack_modifier       INTEGER NOT NULL,
    grapple_hide_modifier INTEGER NOT NULL,
    dimension_min         REAL    NOT NULL,
    dimension_max         REAL    NOT NULL,
    weight_min            REAL    NOT NULL,
    weight_max            REAL    NOT NULL,
    space                 REAL    NOT NULL,
    reach_tall            REAL    NOT NULL,
    reach_long            REAL    NOT NULL
);


CREATE TABLE monster_types
(
    monster_type     TEXT NOT NULL PRIMARY KEY,
    type_description TEXT NOT NULL
) WITHOUT ROWID;

-- Use name for dictionary.
-- When in game, and table will be used that has uuid for each monster.
-- Speed is one land.  monster_movements will be used for other types an limits.
CREATE TABLE monsters
(
    name                 TEXT    NOT NULL PRIMARY KEY,
    size                 TEXT    NOT NULL REFERENCES size_information (size),
    sub_type             TEXT    NOT NULL REFERENCES monster_types (monster_type),
    hit_die_rolls        INTEGER NOT NULL,
    hit_die_sides        INTEGER NOT NULL,
    hit_die_bonus_points INTEGER NOT NULL,
    initiative           INTEGER NOT NULL,
    speed                INTEGER NOT NULL,
    armor_class          INTEGER NOT NULL,
    base_attack          INTEGER NOT NULL,
    grapple              INTEGER NOT NULL,
    attack               INTEGER NOT NULL
) WITHOUT ROWID;

CREATE TABLE monster_movements
(
    name   TEXT    NOT NULL PRIMARY KEY REFERENCES monsters (name),
    burrow INTEGER NOT NULL,
    climb  INTEGER NOT NULL,
    fly    INTEGER NOT NULL,
    swim   INTEGER NOT NULL
) WITHOUT ROWID;

CREATE TABLE monster_attack
(
    name   TEXT NOT NULL REFERENCES monsters (name),
    weapon TEXT NOT NULL REFERENCES weapons (weapon),
    CONSTRAINT monster_weapon PRIMARY KEY (name, weapon)
);

CREATE TABLE monster_descriptions
(
    name    TEXT NOT NULL PRIMARY KEY REFERENCES monsters (name),
    general TEXT NOT NULL,
    combat  TEXT NOT NULL,
    society TEXT NOT NULL
);

CREATE TABLE monster_skills
(
    name     TEXT    NOT NULL PRIMARY KEY REFERENCES monsters (name),
    skill_id INTEGER NOT NULL REFERENCES skills (skill_id)

) WITHOUT ROWID;
-- Indexes
CREATE UNIQUE INDEX idx1 ON classes (char_class);
CREATE UNIQUE INDEX idx2 ON races (race);
CREATE UNIQUE INDEX idx3 ON characters (uuid);
CREATE UNIQUE INDEX idx4 ON monsters (name);
CREATE UNIQUE INDEX idx5 ON monster_attack (name, weapon);


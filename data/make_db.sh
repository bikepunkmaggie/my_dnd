#!/bin/sh

# sqlite3 data.db < class_data.sql


# Path to your SQLite database file
DB_FILE="data.db"

# Check if the database file exists
if [[ ! -f "$DB_FILE" ]]; then
  echo "Database file '$DB_FILE' not found!"
  exit 1
fi

# List all table names
TABLES=$(sqlite3 "$DB_FILE" "SELECT name FROM sqlite_master WHERE type='table' AND name NOT LIKE 'sqlite_%';")

# fks off
sqlite3 "$DB_FILE" "PRAGMA foreign_keys = OFF;"

# Drop each table
for TABLE in $TABLES; do
  echo "Dropping table: $TABLE"
  sqlite3 "$DB_FILE" "DROP TABLE IF EXISTS \"$TABLE\";"
done

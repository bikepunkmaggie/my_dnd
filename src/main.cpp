/**
 * @mainpage
 *
 * @section intro_sec Introduction
 *
 * See README
 */

#include "main.h"    // IWYU pragma: keep

/**
 * Entry point.
 * No args at this time
 *
 * @param argc
 * @param argv
 *
 * @return Standard return.
 */
int main(int argc, char* argv[])
{
    // Set up the QT environment and then proceed
    QApplication app_main(argc, argv);

    // QString configDir =
    // QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
    // qDebug() << "Configuration directory:" << configDir;
    // qDebug() << QApplication::applicationName();

    // Open database
    //@TODO: fix
    const QString  data_path = "/home/mbenton/projects/my_dnd/data/data.db";
    my_dnd::DB_ops db_ops;
    db_ops.open_db(data_path);

    // Ready to do window and go.
    mydnd_gui::Main_window main_window;

    main_window.show();

    return QApplication::exec();
}

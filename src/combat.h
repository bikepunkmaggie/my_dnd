#ifndef MY_DND_COMBAT_H
#define MY_DND_COMBAT_H
#include "character.h"
#include "my_dnd.h"
#include "my_dnd_constants.h"

#include <QString>
#include <algorithm>

namespace mydnd_combat {

using my_dnd::Attrib_value_type;

Attrib_value_type calculate_armor_class();
}    // namespace mydnd_combat
#endif    // COMBAT_H

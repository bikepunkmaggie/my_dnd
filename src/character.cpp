#include "character.h"

namespace mydnd_character {

/**
 * @brief calculate_number_new_skills
 *
 * @param character_in race, class, level is needed.
 *
 * @return calculated skill points to use for purchase
 */
int calculate_number_new_skills(const Character_information& character_in)
{
    auto base_value {
        character_in
            .get_attribute(my_dnd::Class_attributes::base_skills_points_attrib)
            .as_int};
    auto bonus_flag {
        character_in.get_attribute(my_dnd::Race_attributes::extra_feat_attrib)
            .as_int};
    auto itl_mod {character_in.get_ability_modifers()[my_dnd::Int].as_int};

    auto skill_points {base_value + itl_mod};

    // New character or level 1?
    if (character_in.get_attribute(my_dnd::Character_attributes::level_char).as_int ==
        my_dnd::start_level) {
        skill_points = (skill_points < 0) ? 0 : skill_points;
        skill_points *= my_dnd::skills_start_multiplier;
        if (bonus_flag)
            skill_points += my_dnd::skills_start_multiplier;
    }
    else {
        if (bonus_flag)
            skill_points +=
                my_dnd::skills_levelup_bonus;    // bonus is for non-level 1
    }
    return skill_points;
}

///
/// \brief calculate_number_new_feats
/// \param character_in for needed data
/// \return calculated number of feats
///
int calculate_number_new_feats(const Character_information& character_in)
{
    auto bonus_flag {
        character_in.get_attribute(my_dnd::Race_attributes::extra_feat_attrib)
            .as_int};
    auto number_feats {my_dnd::start_level};

    if (bonus_flag) {
        number_feats++;
    }

    auto class_bonus_feat {
        character_in.get_attribute(my_dnd::Class_attributes::bonus_feat_attrib)
            .as_int};
    if (class_bonus_feat) {
        number_feats++;
    }

    return number_feats < 0 ? 0 : number_feats;
}

struct Race_picture {
    QString race_name;
    QString race_pic_base;
};

QString set_character_qpix(const QString& race_in, const QString& sex)
{
    std::vector<Race_picture> race_pictures {
        {"Dwarf", "dwarf"},       {"Human", "human"}, {"Elf", "elf"},
        {"Half-Orc", "half_orc"}, {"Gnome", "gnome"}, {"Half-Elf", "half_elf"},
        {"Halfling", "halfling"}};
    QString temp;
    for (const auto& i : race_pictures)
        if (i.race_name.contains(race_in))
            temp = i.race_pic_base;

    if (sex == "Female") {
        return temp + "_female.png";
    }
    return temp + "_male.png";
}

///
/// Calculate next experience level
/// \param current_level is current level
/// \return experience needed to reach next level
///
int calculate_next_level(int current_level)
{
    my_dnd::ability_score_type next_level {0};
    for (auto i = my_dnd::start_level; i <= current_level; ++i) {
        next_level = next_level + (i * my_dnd::experience_multipler);
        // if (i==my_dnd::start_level){next_level=0;}
    }

    return next_level;
}

///
/// For the 9 spell levels, calculates the number of bonus spells.
/// Table 1-1.
/// \param spell_level
/// \param ability_score
/// \returns number (0 or greater) of bonus spell for the level
///
int calculate_bonus_spell(int spell_level, int ability_score)
{
    auto mod_total {0.0};

    if (!my_dnd::is_even(ability_score))
        --ability_score;

    auto level_adjust {(ability_score + my_dnd::bonus_spell_offset) -
                       (spell_level * my_dnd::bonus_spell_multiplier)};

    if (my_dnd::is_even(level_adjust))
        mod_total = (level_adjust / 2 - my_dnd::mod_adjust);
    else
        mod_total = ((level_adjust - 1) / 2 - my_dnd::mod_adjust);

    if (mod_total > 0)
        return std::ceil(mod_total / my_dnd::bonus_spell_numerator);
    return 0;
}

///
///  Determines number of bonus spells for a race.
///  \param class_in The given class
///  \param scores All ability scores
///  \return The bonus 0 on up.  Table 1-1 is reference.
///  \ref calculate_bonus_spell
///
int get_bonus_spell(const QString& class_in, const Ability_type& scores)
{
    constexpr auto min_spell_level {1};
    if (class_in.isEmpty())
        return 0;
    // Determine which ability score to use, then calculate;
    if (my_dnd::spell_itl_bonus_class.contains(class_in))
        return calculate_bonus_spell(min_spell_level, scores[my_dnd::Int].as_int);

    else if (my_dnd::spell_wis_bonus_class.contains(class_in))
        return calculate_bonus_spell(min_spell_level, scores[my_dnd::Wis].as_int);

    else if (my_dnd::spell_cha_bonus_class.contains(class_in))
        return calculate_bonus_spell(min_spell_level, scores[my_dnd::Cha].as_int);

    // No matter what, 0.
    return 0;
}

my_dnd::attribute_type roll_ability_score()
{
    my_dnd::Dice_roll_info t {.rolls = my_dnd::ability_rolls,
                              .sides = my_dnd::default_die_side};
    return my_dnd::roll_drop_lowest(t);
}

/**
 * For a player character, calculates starting age.
 *
 * @param class_in
 * @param race
 *
 * @return the random starting age.
 */
auto calculate_age(const QString& class_in, const QString& race)
{
    const int roll {my_dnd::get_starting_age(race, class_in)};
    return roll;
}

/**
 * Age, Height, and Weight based on Tables 6-4 and 6-6.
 * Unique to new character generation.
 *
 * @param character_data to extract current data and return
 */
void calculate_vital_statistics(Character_information& character_data)
{
    // Get race and sex ready to set up male mods if needed.

    if (const auto race {character_data.get_race_name()}; !race.isEmpty()) {
        // Sex
        auto sex_in {
            character_data.get_attribute(my_dnd::Character_attributes::gender_char)
                .as_string};
        if (sex_in.isEmpty())
            sex_in = "Male";

        // Will be using a lot so might as will bring in.
        using enum my_dnd::Character_attributes;

        // Weight & Height
        my_dnd::Rolled_height_weight h_w;
        h_w = my_dnd::get_starting_height_weight(race, sex_in);
        character_data.set_attribute(h_w.weight, weight_char);
        character_data.set_attribute(h_w.height, height_char);

        // Age is race and class dependent.
        if (const auto class_in {character_data.get_class_type_name()};
            !class_in.isEmpty())
            character_data.set_attribute(calculate_age(class_in, race), age_char);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Class member functions
////////////////////////////////////////////////////////////////////////////////

/// TODO: implement
Character_information::Character_information()
{
    // ability modifiers.
    // r/c/e stuff.
}
void Character_information::load_a_character(const QString& uuid)
{
    my_dnd::Character_database_data load_char;
    load_char = my_dnd::get_character(uuid);
    character_multi_pass.id.set_avt(load_char.uuid);
    character_multi_pass.name.set_avt(load_char.name);
    level.set_avt(load_char.level);
    xp.set_avt(load_char.xp);
    age.set_avt(load_char.age);
    height.set_avt(load_char.height);
    weight.set_avt(load_char.weight);
    alignment.set_avt(load_char.alignment);
    race.set_avt(load_char.race);
    char_class.set_avt(load_char.char_class);
    deity.set_avt(load_char.deity);
    sex.set_avt(load_char.sex);
    eye_color.set_avt(load_char.eye_color);
    skin_color.set_avt(load_char.skin_color);
    new_character =
        load_char.new_character;    // Can load any were, so set true if in progress?

    abilities.set_ability(my_dnd::Str, load_char.strength);
    abilities.set_ability(my_dnd::Dex, load_char.dexterity);
    abilities.set_ability(my_dnd::Con, load_char.constitution);
    abilities.set_ability(my_dnd::Int, load_char.intelligence);
    abilities.set_ability(my_dnd::Wis, load_char.wisdom);
    abilities.set_ability(my_dnd::Cha, load_char.charisma);
}
/**
 * Generate a new character--roll for abilities. if not set to true, will bomb.
 *
 * @note Not fully implemented
 */
void Character_information::make_new_character()
{
    new_character = true;
    level.set_avt(my_dnd::start_level);
    character_multi_pass.id.set_avt(my_dnd::get_uuid());
}

/**
 * Save the character as new, not a final.
 */
void Character_information::save_character()
{
    my_dnd::Character_database_data temp;
    temp.uuid          = this->character_multi_pass.id.as_string,
    temp.name          = this->character_multi_pass.name.as_string;
    temp.level         = 1;
    temp.xp            = 0;
    temp.strength      = this->abilities[my_dnd::Str].as_int;
    temp.dexterity     = this->abilities[my_dnd::Dex].as_int;
    temp.constitution  = this->abilities[my_dnd::Con].as_int;
    temp.intelligence  = this->abilities[my_dnd::Int].as_int;
    temp.wisdom        = this->abilities[my_dnd::Wis].as_int;
    temp.charisma      = this->abilities[my_dnd::Cha].as_int;
    temp.age           = this->age.as_int;
    temp.height        = this->height.as_int;
    temp.weight        = this->weight.as_int;
    temp.alignment     = this->alignment.as_string;
    temp.race          = this->race.as_string;
    temp.char_class    = this->char_class.as_string;
    temp.deity         = this->deity.as_string;
    temp.sex           = this->sex.as_string;
    temp.eye_color     = "brown";
    temp.skin_color    = "tan";
    temp.new_character = true;

    my_dnd::save_a_character(temp);
}

///
/// Generic get for any character attribute.
/// \ref character_multi_pass, \ref race, and \ref  char_class are best though
/// direct gets. \param which_to_get \return The attribute requested--only one.
///
Attrib_value_type Character_information::get_attribute(
    const my_dnd::Character_attributes& which_to_get) const
{
    switch (which_to_get) {
        using enum my_dnd::Character_attributes;
        // case Character_attributes::name_char:
        //   return name;
    case race_char:
        return race;
    case class_type_char:
        return char_class;
    case deity_char:
        return deity;
    case gender_char:
        return sex;
    case alignment_char:
        return alignment;
    case level_char:
        return level;
    case age_char:
        return age;
    case height_char:
        return height;
    case weight_char:
        return weight;
    case name_char:
        return character_multi_pass.name;
    case id_char:
        return character_multi_pass.id;
    case str_char:
        return abilities[my_dnd::Str];
    case dex_char:
        return abilities[my_dnd::Dex];
    case con_char:
        return abilities[my_dnd::Con];
    case itl_char:
        return abilities[my_dnd::Int];
    case wis_char:
        return abilities[my_dnd::Wis];
    case cha_char:
        return abilities[my_dnd::Cha];
    }
    return Attrib_value_type {};
}

void Character_information::set_attribute(
    const QVariant& value_in, const my_dnd::Character_attributes& which_to_set)
{
    switch (which_to_set) {
        using enum my_dnd::Character_attributes;
    case name_char:
        character_multi_pass.name.set_avt(value_in.toString());
        break;
    case race_char:
        race.set_avt(value_in.toString());
        qpix.set_avt(set_character_qpix(race.as_string, sex.as_string));
        // if (!abilities.empty())
        // update_abilities_for_race();
        break;
    case class_type_char:
        char_class.set_avt(value_in.toString());
        break;
    case deity_char:
        deity.set_avt(value_in.toString());
        break;
    case gender_char:
        sex.set_avt(value_in.toString());
        qpix.set_avt(set_character_qpix(race.as_string, sex.as_string));
        break;
    case alignment_char:
        alignment.set_avt(value_in.toString());
        break;
    case id_char:
        character_multi_pass.id.set_avt(value_in.toString());
        break;
    case level_char:
        level.set_avt(value_in.toInt());
        break;
    case age_char:
        age.set_avt(value_in.toInt());
        break;
    case height_char:
        height.set_avt(value_in.toInt());
        break;
    case weight_char:
        weight.set_avt(value_in.toInt());
        break;
    case str_char:
        abilities.set_ability(my_dnd::Str, value_in.toInt());
        break;
    case dex_char:
        abilities.set_ability(my_dnd::Dex, value_in.toInt());
        break;
    case con_char:
        abilities.set_ability(my_dnd::Con, value_in.toInt());
        break;
    case itl_char:
        abilities.set_ability(my_dnd::Int, value_in.toInt());
        break;
    case wis_char:
        abilities.set_ability(my_dnd::Wis, value_in.toInt());
        break;
    case cha_char:
        abilities.set_ability(my_dnd::Cha, value_in.toInt());
        break;
    default:
        break;
    }
}

///
/// Sets modifiers and associated attributes as needed.
/// \todo Add more as work is done.
///
void Character_information::update_modifiers()
{
    if (new_character)    // With a new char, need to update based on race
        update_race_info();
    ability_modifiers.set_ability_modifier(abilities);

    set_armor_class();
}

///
/// Updates the race.
/// When creating a character, \ref race can be null, so a quick check is done.
///
void Character_information::update_race_info()
{
    if (race.as_string.isEmpty())
        return;    // Nothing to see here.

    // Get the race info for the selected race
    // My use a few times, keep between selections.
    static Race_standard race_temp;
    race_info = race_temp[race.as_string];
}

///
/// Sets all aspects of armor class.
/// \todo Add rest once implemented.
///
void Character_information::set_armor_class()
{
    auto total_ac {my_dnd::base_armor_class};    // start with base.
    int  size_bonus {
        race_info.get_race_attribute(my_dnd::Race_attributes::small_size_bonus_attrib)
            .as_int};
    armor_class.size_modifer.set_avt(size_bonus);
    // armor_class.size_modifier.set_avt((size_bonus == true ? 1 : 0));
    armor_class.dexterity_modifer.set_avt(ability_modifiers[my_dnd::Dex].as_int);
    total_ac +=
        armor_class.dexterity_modifer.as_int + armor_class.size_modifer.as_int;
    armor_class.total_ac.set_avt(total_ac);
}
///
/// Loads and writes playable character data.
/// Data is in a vector of type Character.
/// \warning Make sure any new character data is saved, as this clears
/// everything.
///
Player_characters::Player_characters()
{
    // Told you so.
    playable_characters.clear();
    playable_characters_list.clear();
    read_characters();

    /// \note  \ref Character_information to complete \ref
    /// playable_characters.
    // read_in_char(playable_characters);

    // Put the race_data into the list.
    std::transform(playable_characters.begin(), playable_characters.end(),
                   std::back_inserter(playable_characters_list),
                   [](Character_information i) { return i.get_multipass(); });
    std::sort(playable_characters_list.begin(), playable_characters_list.end());
}

///
/// Character_information::operator []
/// \param character_id_in
/// \return the \ref Character_information of the character_id.
///
Character_information& Player_characters::operator[](const QString& character_id_in)
{
    // Find the attribute passed an attribute enum class
    auto t =
        std::find_if(playable_characters.begin(), playable_characters.end(),
                     [&cm = character_id_in](const Character_information& m) -> bool {
                         if (QString::compare(cm, m.get_character_id()) == 0)
                             return true;
                         return false;
                     });
    if (t == std::end(playable_characters))
        mydnd_err::error("Not a valid race in Playable_characters::operator[]");

    return *t;
}

///
/// Add a character to the database.
/// \param new_char
/// \todo Check for valid!
///
void Player_characters::add_character(const Character_information& new_char)
{
    playable_characters.push_back(new_char);
}

///
/// Generic get for any character misc attribute.
/// \param which_to_get
/// \return The attribute requested--only one.
/// \note Returns a vector.
/// \note For /ref deity use /ref get_deities.
///
std::vector<Attrib_value_type> Character_misc_standard::
    get_character_misc_information(
        const my_dnd::Character_misc_attributes& which_to_get) const
{
    // TODO update
    std::vector<Attrib_value_type> temp {};
    switch (which_to_get) {
        using enum my_dnd::Character_misc_attributes;
    case alignment_char:
        return alignments;
    case sexes_char:
        return sexes;
    case eye_colors_char:
        return eye_colors;
    case hair_colors_char:
        return hair_colors;
    case skin_colors_char:
        return skin_colors;
    case deity_char:    // TODO implement
        return deity;
    case gold_char:    // TODO implement
        return temp;
    };

    return temp;
}

/**
 * Handle any changes in class.  Not all will change, so simple return if not.
 */
std::vector<Attrib_value_type> Character_misc_standard::
    get_character_misc_information(
        const my_dnd::Character_misc_attributes& which_to_get, const QString class_in,
        const QString race_in)
{

    std::vector<Attrib_value_type> temp {};

    Attrib_value_type attrib_temp;    // most will use
    switch (which_to_get) {
        using enum my_dnd::Character_misc_attributes;
    case alignment_char: {
        this->alignments.clear();
        for (auto t = my_dnd::get_alignments(class_in); const auto& i : t) {
            attrib_temp.set_avt(i);
            alignments.push_back(attrib_temp);
        }
        return alignments;
    }
    case sexes_char:
        return sexes;
    case eye_colors_char:
        return eye_colors;
    case hair_colors_char:
        return hair_colors;
    case skin_colors_char:
        return skin_colors;
    case deity_char:    // TODO implement
    {
        deity.clear();
        if (QString::compare("Cleric", class_in) == 0 && !race_in.isEmpty()) {

            for (auto t = my_dnd::get_cleric_deities(race_in); const auto& i : t) {
                attrib_temp.set_avt(i);
                deity.push_back(attrib_temp);
            }
        }
        else {
            for (auto t = my_dnd::get_deities(); const auto& i : t) {
                attrib_temp.set_avt(i);
                deity.push_back(attrib_temp);
            }
        }
        return deity;
    }
    case gold_char:    // TODO implement
        return temp;
    };

    return temp;
}

/**
 * Instantiate with all values possible.  Update later as class or race is
 * selected.
 */
Character_misc_standard::Character_misc_standard()
{

    Attrib_value_type attrib_temp;    // most will use
    using enum my_dnd::Character_misc_attributes;
    for (auto temp = my_dnd::get_character_alignments(); const auto& i : temp) {
        attrib_temp.set_avt(i);
        alignments.push_back(attrib_temp);
    }
    for (auto temp = my_dnd::get_all_sex_query(); const auto& i : temp) {
        attrib_temp.set_avt(i);
        sexes.push_back(attrib_temp);
    }
    for (auto temp = my_dnd::get_deities(); const auto& i : temp) {
        attrib_temp.set_avt(i);
        deity.push_back(attrib_temp);
    }
}
}    // namespace mydnd_character

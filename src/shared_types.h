#ifndef SHARED_TYPES_H
#define SHARED_TYPES_H

#include <QString>
#include <QUuid>
#include <random>
#include <utility>

namespace my_dnd {

using die_type       = int;
using attribute_type = die_type;    ///< Attributes and such are die types

const QString empty_string {QString()};
/// Six sides is standard
static constexpr die_type default_die_side {6};
/// Rolls typically start at one.
static constexpr die_type default_die_start {1};

/// Set default roll to one roll
static constexpr die_type default_number_of_rolls {1};
static constexpr die_type max_rolls {100};
static constexpr die_type min_rolls {1};
static constexpr die_type min_die_size {1};
static constexpr die_type max_die_size {100};

/**
 * A complete structure to hold all that is needed to roll.
 *
 * @note No checks here.  Done by roller.
 */
struct Dice_roll_info {
    die_type rolls;
    die_type sides;
};

QString  get_uuid();
die_type roll_die(const Dice_roll_info&);
die_type roll_drop_lowest(const Dice_roll_info& Dice_roll_info);

/// When taken as a whole, the lowest modification total.
///  Details on Page 8, re-rolling.
constexpr auto low_mods {0};

/// For checking mod adjustments, use 5 to subtract from a whole number
/// See \ref mydnd_character::check_modifier.
constexpr auto mod_adjust {5};

/// In a vector the position of the strength ability.
constexpr auto Str {0};
/// In a vector the position of the dexterity ability.
constexpr auto Dex {1};
/// In a vector the position of the constitution ability.
constexpr auto Con {2};
/// In a vector the position of the intelligence ability.
/// @note the abbreviation is not the standard int, but itl.
constexpr auto Int {3};
/// In a vector the position of the wisdom ability.
constexpr auto Wis {4};
/// In a vector the position of the charisma ability.
constexpr auto Cha {5};
/// The number of abilities per the manual.
constexpr auto num_abilities {6};

constexpr auto Roll_1 {0};
constexpr auto Roll_2 {1};
constexpr auto Roll_3 {2};
constexpr auto Roll_4 {3};
constexpr auto Roll_5 {4};
constexpr auto Roll_6 {5};

/// The number of rolls allowed when creating a character (drop the lowest)
/// Details on Page 7, Ability scores
constexpr auto ability_rolls {4};

struct Monster_database_data {
    QString name {empty_string};
    QString size {empty_string};
    QString sub_type {empty_string};
    int     hit_die_rolls {0};
    int     hit_die_sides {0};
    int     hit_die_bonus_points {0};
    int     initiative {0};
    int     speed {0};
    int     armor_class {0};
    int     base_attack {0};
    int     grapple {0};
    int     attack {0};
};

/**
 * Non land speed movements.
 */
struct Monster_movements {
    QString name {empty_string};
    int     burrow {0};
    int     climb {0};
    int     fly {0};
    int     swim {0};
};

struct Weapons_database_data {
    QString weapon {empty_string};
    QString proficiency {empty_string};
    QString combat_type {empty_string};
    QString encumbrance {empty_string};
    QString type {empty_string};
    QString type_or {empty_string};
    QString type_and {empty_string};
    QString cost {empty_string};
    int     damage_small_die_rolls {0};
    int     damage_small_die_sides {0};
    int     damage_medium_die_rolls {0};
    int     damage_medium_die_sides {0};
    int     double_damage_small_die_rolls {0};
    int     double_damage_small_die_sides {0};
    int     double_damage_medium_die_rolls {0};
    int     double_damage_medium_die_sides {0};
    int     critical {0};
    int     threat {0};
    int     range_increment {0};
    int     weight {0};
};

/**
 * For @ref Attrib_value_type, the value of the string.
 */

inline const auto attrib_default_string = QStringLiteral("Empty Attribute");

/**
 *  Default @ref Attrib_value_type attribute value for integer values.
 */
constexpr auto as_int_default {0};

/**
 * Main storage struct for attributes.
 * Holds strings, ints and bools.  To call just do as_...
 * bool, use as_int or as_string with "True" or "False"
 * Use provided setter and getters.
 * For strings, the @ref as_int_default will be default.
 * With a supplied @ref attribute_type and a pre_pend, you can set the string with a
 * prepend value.  For example for a roll of 15 and 16, you can set to 'Roll 1: 15'
 * and 'Roll 2: 16'. with set_avt(15,"Roll 1: ") ...
 * @note @ref as_string will always be @ref attrib_default_string is not set
 */
struct Attrib_value_type {
    QString        as_string {empty_string};
    attribute_type as_int {as_int_default};

    // set functions, get is just access
    // Sets handle the various changes needed
    void set_avt(const QString& a)
    {
        as_string = a;
    }
    void set_avt(const attribute_type a)
    {
        as_string = QString::number(a);
        as_int    = a;
    }
    [[maybe_unused]] void set_vt(const bool a)
    {
        as_int = a;
        if (a)
            as_string = "True";
        else
            as_string = "False";
    }
    void set_avt(const attribute_type a, const QString& pre_pend)
    {
        as_string = pre_pend + QString::number(a);
        as_int    = a;
    }

    friend bool operator<(const Attrib_value_type& l, const Attrib_value_type& r)
    {
        return l.as_int < r.as_int;
    }
};
}    // namespace my_dnd

namespace mydnd_character {

/**
 * Abilities and modifications.
 *
 * @note for rolling a new character, the sdciwc order is used.  Confusing and
 * prone to bugs.
 *
 * @bug Fix the note.
 */
class Ability_type {
public:
    Ability_type();
    void clear_data();
    int  max() const;

    /**
     * Clear and set all to 0.
     */
    void reset_ability_vector();

    // Overload operator[] for getter
    const my_dnd::Attrib_value_type& operator[](size_t index) const;

    void set_ability(int, int);
    void set_ability(int, QString);
    void set_ability_negative(int, int);
    int  size(void);
    bool is_empty()
    {
        return abilities.empty();
    }
    int get_ability(int what)
    {
        return abilities[what].as_int;
    };

    bool check_ability(const Ability_type& abilities_in);

private:
    std::vector<my_dnd::Attrib_value_type> abilities;
};

class Ability_modifiers : public Ability_type {
public:
    Ability_modifiers() {};
    bool check_min_modifer();
    void set_ability_modifier(Ability_type);

private:
    int calculate_ability_modifier(int ability);
};

class Rolled_abilities : public Ability_type {
public:
    Rolled_abilities();
    void reroll();

private:
    my_dnd::die_type roll_ability_score(const my_dnd::Dice_roll_info&);

    std::vector<my_dnd::Attrib_value_type> rolled_abilities;
};

}    // namespace mydnd_character

#endif    // SHARED_TYPES_H

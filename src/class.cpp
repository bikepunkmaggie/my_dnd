//
// Created by mbenton on 11/22/24.
//

#include "class.h"
namespace mydnd_character {

// Pretty placeholder
Class_information::Class_information()
{
}
Class_information::Class_information(const QString& class_in)
{
    this->set_class_type_name(class_in);
    my_dnd::Class_descriptions desc = my_dnd::get_class_descriptions(class_in);
    this->class_type_brief.set_avt(desc.general);
    this->class_type_description.set_avt(desc.characteristics);

    my_dnd::Base_class_traits temp = my_dnd::get_base_class_traits(class_in);
    this->hit_die.set_avt(temp.hit_die);
    this->base_skill_points.set_avt(temp.skill_per_level);
    this->gold_rolls.set_avt(temp.start_gold_rolls);
}

///
/// Returns class attribute.
/// For \ref class_type, use \ref get_class_type_name
/// \param which_to_get
/// \return \ref my_dnd::Attrib_value_type
/// \note \ref my_dnd::attrib_default_string for value if not set.
///
Attrib_value_type Class_information::get_class_attribute(
    const my_dnd::Class_attributes& which_to_get) const
{
    switch (which_to_get) {
        using enum my_dnd::Class_attributes;
    case class_attrib:
        return class_type;
    case hit_die_attrib:
        return hit_die;
    case align_restriction_attrib:
        return alingment_restriction;
    case class_brief_attrib:
        return class_type_brief;
    case class_description_attrib:
        return class_type_description;
    case deity_restriction_attrib:
        return diety_restrictions;
    case damage_reduction_attrib:
        return damage_reduction;
    case base_skills_points_attrib:
        return base_skill_points;
    case bonus_feat_attrib:
        return bonus_feat_flag;
    case bonus_feat_level_up_pattern_attrib:    // Dummy
        return Attrib_value_type {};
        break;
    case bonus_feat_limit_attrib:    // Dummy
        return Attrib_value_type {};
        break;
    }
    return Attrib_value_type {};
}

///
/// Create information about all classes
/// Used during character creation as player characters need this information
/// \ref Class_information
///
Class_standard::Class_standard()
{
    // Fill in class names based on class_data;
    class_list.clear();

    // Get list of classes, and fill data.
    for (auto r = my_dnd::get_classes(); auto i : r) {
        this->class_list.push_back(i);
        Class_information temp(i);
        this->class_data.push_back(temp);
    }
}

///
/// Contains all the information on class.
/// Used as reference when creating a character.
/// For in play, the \ref Player_characters an \ref Character_information class
/// is used. Call is simply using [class_name] to get information about a class.
/// \ref class_list is a list of races for use in selectors.
/// \ref class_data is a vector of \ref Class_information for each class.
/// \param class_in
///
Class_information& Class_standard::operator[](const QString& class_in)
{
    // Find the attribute passed an attribute enum class
    auto t = std::find_if(class_data.begin(), class_data.end(),
                          [&cm = class_in](const Class_information& m) -> bool {
                              if (QString::compare(cm, m.get_class_type_name()) == 0)
                                  return true;
                              return false;
                          });
    if (t == std::end(class_data))
        mydnd_err::error("Not a valid class in class[]");

    return *t;
}
}    // namespace mydnd_character

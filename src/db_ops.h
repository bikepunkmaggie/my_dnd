/**
 * @file
 */

#ifndef DB_OPS_H
#define DB_OPS_H

#include "../gui/error_and_debug.h"    // IWYU pragma: keep
#include "shared_types.h"

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

namespace my_dnd {
//====================================================================================
// Misc. Constants
/**
 * Neutral is filtered out for player characters.
 * If implementation, than the database would need updated.
 */
static const QString Neutral_Filter {"Neutral"};

//====================================================================================
// Forward Declarations
struct Character_database_data;
struct Rolled_height_weight;
struct Base_class_traits;
struct Base_race_traits;
struct Race_descriptions;
struct Class_descriptions;

//====================================================================================
// Function Declarations
QString get_rand_name(const QString&, const QString&);

Base_class_traits  get_base_class_traits(const QString&);
Class_descriptions get_class_descriptions(const QString&);
Race_descriptions  get_race_descriptions(const QString&);
Base_race_traits   get_base_race_traits(const QString&);

std::vector<QString>    get_all_sex_query();
std::vector<QString>    get_races();
std::vector<QString>    get_classes();
std::vector<QString>    get_deities();
std::vector<QString>    get_cleric_deities(const QString&);
std::vector<QString>    get_character_alignments();
std::vector<QString>    get_alignments(const QString&);
Character_database_data get_character(const QString&);
Rolled_height_weight    get_starting_height_weight(const QString&, const QString&);
int                     get_starting_age(const QString&, const QString&);
void                    get_new_char(mydnd_character::Ability_type&,
                                     mydnd_character::Ability_type& index_in, const QString);
void                    save_a_character(const Character_database_data&);
void                    save_new_char(const mydnd_character::Ability_type&,
                                      const mydnd_character::Ability_type&, const QString&);

std::vector<Monster_database_data> get_monster_data();
std::vector<Weapons_database_data> get_weapons();

const QString no_where_statement {QString()};
//====================================================================================
// Structures

/**
 * For age calculations.
 */
struct Age_calculation_data {
    int adulthood {0};
    int rolls {0};
    int die {0};
};
/**
 * Fill and send to my_dnd::save_a_character to save a character.
 *
 * @note Values are not checked.
 */
struct Character_database_data {
    QString uuid {QUuid().toString()};
    QString name {"Blank Name"};
    QString race {"Human"};
    int     level {0};
    int     xp {0};
    int     strength {9};
    int     dexterity {8};
    int     constitution {7};
    int     intelligence {5};
    int     wisdom {6};
    int     charisma {18};
    int     age {28};
    int     height {69};
    int     weight {150};
    QString alignment {"Neutral"};
    QString char_class {"Thief"};
    QString deity {"no name"};
    QString sex {"Male"};
    QString eye_color {"blue"};
    QString skin_color {"tan"};
    bool    new_character {true};
};
/**
 * Used for saving a new character (in progress).
 *
 * @note Values are not checked.
 */
struct New_character_database_data {
    QString uuid {QUuid().toString()};
    int     roll_1 {0};
    int     roll_2 {0};
    int     roll_3 {0};
    int     roll_4 {0};
    int     roll_5 {0};
    int     roll_6 {0};
    int     str_index {0};
    int     dex_index {0};
    int     con_index {0};
    int     int_index {0};
    int     wis_index {0};
    int     cha_index {0};
};

/**
 * Used during character creation.
 */
struct Starting_height_weight_data {
    QString race {"Human"};
    QString sex {"Male"};
    int     base_height {0};
    int     height_rolls {0};
    int     height_die {0};
    int     base_weight {0};
    int     weight_rolls {0};
    int     weight_die {0};
};

/**
 * @todo implement
 */
struct Alignments_data {
    QString alingment {QString()};
    QString nickname {QString()};
    QString description {QString()};
    bool    character_alignment {true};
};

/**
 * Simple base traits for a new character.
 */
struct Base_class_traits {
    QString char_class {QString()};
    int     hit_die {0};
    int     skill_per_level {0};
    int     start_gold_rolls {0};
};

/**
 * Return rolled values of each height and weight during character creation.
 */
struct Rolled_height_weight {
    int height {0};
    int weight {0};
};

/**
 *Used to pull in data from said table.
 */
struct Base_race_traits {
    QString race {QString()};
    QString size {QString()};
    int     extra_feat {0};
    int     land_speed {0};
    int     str_adj {0};
    int     dex_adj {0};
    int     con_adj {0};
    int     itl_adj {0};
    int     wis_adj {0};
    int     chr_adj {0};
};

/**
 * All description levels of a race.
 */
struct Race_descriptions {
    QString race {QString()};
    QString alignment {QString()};
    QString adventures {QString()};
    QString general {QString()};
    QString lands {QString()};
    QString language {QString()};
    QString names {QString()};
    QString personality {QString()};
    QString physical {QString()};
    QString relations {QString()};
    QString religion {QString()};
};

/**
 * ALl character class descriptions for a characters class.
 */
struct Class_descriptions {
    QString char_class {"Thief"};
    QString alignment {QString()};
    QString adventures {QString()};
    QString background {QString()};
    QString characteristics {QString()};
    QString general {QString()};
    QString other_classes {QString()};
    QString races {QString()};
    QString religion {QString()};
    QString role {QString()};
};

/**
 * Simple class to open and close a database.
 * The program is expected to use one database.
 */
class DB_ops {
public:
    explicit DB_ops() = default;
    void open_db(const QString& path);

    ~DB_ops()
    {
        db.close();
        QSqlDatabase::removeDatabase(database_name);
    }

private:
    /// Keep name same as program
    QString database_name {"my_dnd"};
    /// Hard coded type, keep SQL simple so should not be a hard push to change.
    QString database_type {"QSQLITE"};
    /// The database set with type.
    QSqlDatabase db {QSqlDatabase::addDatabase(database_type)};
    /// The database location
    QString db_file {};
};

}    // namespace my_dnd

#endif    // DB_OPS_H

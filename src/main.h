#ifndef MYDND_MAIN_H
#define MYDND_MAIN_H

#include "../gui/char_gen.h"    // IWYU pragma: keep
#include "../gui/error_and_debug.cpp"
#include "../gui/main_window.h"    // IWYU pragma: keep
#include "character.h"             // IWYU pragma: keep
#include "config.h"
#include "db_ops.h"
#include "my_dnd.h"          // IWYU pragma: keep
#include "my_dnd_enums.h"    // IWYU pragma: keep

#include <QApplication>

#ifdef _WIN32
import std;
#elif __linux__
#    include <iostream>    // IWYU pragma: keep    //right now debug
#    include <memory>      // IWYU pragma: keep
#    include <vector>      // IWYU pragma: keep    //vector
#else
#    error OS Not supported
#endif

#endif    // MYDND_MAIN_H

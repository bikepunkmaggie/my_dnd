/**
 * @file
 *
 * Functions and class implementations used for monster classes and
 * monster operations.
 *
 * @author Mike H Benton
 *
 * @date 2025-01-25
 */
#include "monsters.h"

/**
 * @namespace mydnd_monsters
 *
 * Holds classes and function particular to monsters.
 */
namespace mydnd_monsters {

/**
 * Constructor will populate with all monster data from the database.
 *
 * @note Clears out all previous data.
 */
Monster_standard::Monster_standard()
{
    monster_data.clear();
    for (auto t {my_dnd::get_monster_data()}; auto const& i : t) {
        using enum my_dnd::Monster_attributes;
        Monster_information temp;
        temp.set_attribute(i.name, monster_name);
        temp.set_attribute(i.size, monster_size);
        monster_data.push_back(temp);
    }
    number_monsters = monster_data.size();
}

/**
 * Set for all attributes, class may have some quick sets but should call this.
 *
 * @param value_in
 * @param which_to_set
 */
void Monster_information::set_attribute(const QVariant&                  value_in,
                                        const my_dnd::Monster_attributes which_to_set)
{
    switch (which_to_set) {
        using enum my_dnd::Monster_attributes;
    case monster_name:
        name.set_avt(value_in.toString());
        break;
    case monster_size:
        size.set_avt(value_in.toString());
        break;
    case monster_type:
        type.set_avt(value_in.toString());
        break;
    default:
        break;
    }
}

/**
 * Set for all attributes, class may have some quick sets but should call this.
 *
 * @param which_to_set
 */
my_dnd::Attrib_value_type Monster_information::get_attribute(
    my_dnd::Monster_attributes which_to_get)
{
    switch (which_to_get) {
        using enum my_dnd::Monster_attributes;
    case monster_name:
        return name;
    case monster_size:
        return size;
    case monster_type:
        return type;
    default:
    }
    return my_dnd::Attrib_value_type();
}

}    // namespace mydnd_monsters

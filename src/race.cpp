//
// Created by mbenton on 11/22/24.
//

#include "race.h"

namespace mydnd_character {

// Pretty placeholder
Race_information::Race_information()
{
}
Race_information::Race_information(const QString& race_in)
{
    this->set_race_name(race_in);

    descriptions = my_dnd::get_race_descriptions(race_in);

    my_dnd::Base_race_traits temp = my_dnd::get_base_race_traits(race_in);
    set_attribute(temp.str_adj, my_dnd::Race_attributes::str_adj_attrib);
    set_attribute(temp.dex_adj, my_dnd::Race_attributes::dex_adj_attrib);
    set_attribute(temp.con_adj, my_dnd::Race_attributes::con_adj_attrib);
    set_attribute(temp.itl_adj, my_dnd::Race_attributes::itl_adj_attrib);
    set_attribute(temp.wis_adj, my_dnd::Race_attributes::wis_adj_attrib);
    set_attribute(temp.chr_adj, my_dnd::Race_attributes::cha_adj_attrib);
    set_attribute(temp.land_speed, my_dnd::Race_attributes::land_speed_attrib);
    set_attribute(temp.size, my_dnd::Race_attributes::creat_size_attrib);
}

///
/// Generic get for any race attribute.
/// \ref race is possible, bus use \ref get_race_name
/// \param which_to_get
/// \return The attribute requested--only one.
///
Attrib_value_type Race_information::get_race_attribute(
    const my_dnd::Race_attributes& which_to_get) const
{
    switch (which_to_get) {
        using enum my_dnd::Race_attributes;
    case race_attrib:
        return race;
    case favored_attrib:
        return favored;
    case addl_lang_attrib:
        return addl_lang;
    case creat_size_attrib:
        return creat_size;
    case str_adj_attrib:
        return str_adj;
    case dex_adj_attrib:
        return dex_adj;
    case con_adj_attrib:
        return con_adj;
    case itl_adj_attrib:
        return itl_adj;
    case wis_adj_attrib:
        return wis_adj;
    case cha_adj_attrib:
        return cha_adj;
    case land_speed_attrib:
        return land_speed;
    case extra_feat_attrib:
        return extra_feat;
    case no_armor_speed_reduction_attrib:
        return no_armor_speed_reduction;
    case darkvision_attrib:
        return darkvision;
    case stonecunning_attrib:
        return stonecunning;
    case low_light_vision_attrib:
        return low_light_vision;
    case small_size_bonus_attrib:
        return small_size_bonus;
    case giant_bonus_attrib:
        return giant_bonus;
    case stability_bonus_attrib:
        return stability_bonus;
    case personality_attrib: {
        Attrib_value_type temp {};
        temp.set_avt(descriptions.personality);
        return temp;
    }
    }
    return Attrib_value_type {};
}

void Race_information::set_attribute(const QVariant&                string_in,
                                     const my_dnd::Race_attributes& attrib)
{
    switch (attrib) {
        using enum my_dnd::Race_attributes;
    case personality_attrib:
        personality.set_avt(string_in.toString());
        break;
    case str_adj_attrib:
        str_adj.set_avt(string_in.toInt());
        break;
    case dex_adj_attrib:
        dex_adj.set_avt(string_in.toInt());
        break;
    case con_adj_attrib:
        con_adj.set_avt(string_in.toInt());
        break;
    case itl_adj_attrib:
        itl_adj.set_avt(string_in.toInt());
        break;
    case wis_adj_attrib:
        wis_adj.set_avt(string_in.toInt());
        break;
    case cha_adj_attrib:
        cha_adj.set_avt(string_in.toInt());
        break;
    case land_speed_attrib:
        land_speed.set_avt(string_in.toInt());
        break;
    case extra_feat_attrib:
        extra_feat.set_avt(string_in.toInt());
        break;
    case creat_size_attrib:
        creat_size.set_avt(string_in.toString());
        break;
    default:;
    }
}

/**
 * Contains all the information on race. Used as reference when creating a character.
 * For in play, the @ref Player_characters an @ref Character_information class is
 * used. Call is simply using [race] to get information about a race. @ref race_list
 * is a list of races for use in selectors. @ref race_data is a vector of @ref
 * Race_information for each race.
 */
Race_standard::Race_standard()
{
    // Only do once, clear out old data before generating again.
    race_data.clear();
    race_list.clear();

    // Get list of races, and fill data.
    for (auto r = my_dnd::get_races(); auto i : r) {
        this->race_list.push_back(i);
        Race_information temp(i);
        this->race_data.push_back(temp);
    }
}

///
/// Race_standard::operator []
/// \param race_in
/// \return the \ref Race_information of the race_in.
///
Race_information& Race_standard::operator[](const QString& race_in)
{
    // Find the attribute passed an attribute enum class
    auto t = std::find_if(race_data.begin(), race_data.end(),
                          [&cm = race_in](const Race_information& m) -> bool {
                              if (QString::compare(cm, m.get_race_name()) == 0)
                                  return true;
                              return false;
                          });
    if (t == std::end(race_data))
        mydnd_err::error("Not a valid race in Race_standard::race[]");

    return *t;
}
}    // namespace mydnd_character
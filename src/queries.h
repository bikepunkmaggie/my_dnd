/**
 *   @file
 *   Keep all query stuff here
 */

#ifndef MY_DND_QUERIES_H
#define MY_DND_QUERIES_H
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QString>
#include <QStringBuilder>
#include <boost/pfr.hpp>
#include <boost/pfr/core.hpp>
#include <boost/pfr/core_name.hpp>
#include <random>
#include <string>
#include <typeinfo>

namespace my_dnd_queries {

constexpr auto Field_Not_Found {-1};

/******************************************************************************/
// Query terms.  Get rid of space errors.
static const QString Select {"SELECT "};    // No space before needed.
static const QString From {" FROM "};
static const QString Where {" WHERE "};
static const QString And {" AND "};
static const QString Insert_or_Replace {"INSERT OR REPLACE INTO "};
static const QString Values {" VALUES "};
static const QString Equals {"="};
static const QString NotEquals {"!="};

/******************************************************************************/
// Tables
static const QString Table_Alignments             = "alignments";
static const QString Table_Allowed_Alignments     = "allowed_alignments";
static const QString Table_Base_Class_Traits      = "base_class_traits";
static const QString Table_Base_Race_Traits       = "base_race_traits";
static const QString Table_Classes                = "classes";
static const QString Table_Class_Descriptions     = "class_descriptions";
static const QString Table_Deities                = "deities";
static const QString Table_Characters             = "characters";
static const QString Table_New_Characters         = "new_characters";
static const QString Table_Races                  = "races";
static const QString Table_Race_Descriptions      = "race_descriptions";
static const QString Table_Sex                    = "sex";
static const QString Table_Starting_Age           = "starting_age";
static const QString Table_Starting_Height_Weight = "starting_height_weight";
static const QString Table_Allowed_Deity          = "allowed_cleric_deity";
static const QString Table_Random_Names           = "random_names";
static const QString Table_Weapons {"weapons"};
static const QString Table_Monsters {"monsters"};

/******************************************************************************/
// Limit and offset
static const QString Limit_1_Offset = " LIMIT 1 OFFSET :o";
static const QString Bind_Offset    = ":o";

/******************************************************************************/
// Field names: Some needed for where building and such.
static const QString Field_Alignment           = "alignment";
static const QString Field_Character_Alignment = "character_alignment";
static const QString Field_Name                = "name";
static const QString Field_Race                = "race";
static const QString Field_Class               = "char_class";
static const QString Field_Deity               = "deity";
static const QString Field_Sex                 = "sex";
static const QString Field_Uuid                = "uuid";

void query_prepare(QSqlQuery&, const QString&);
void query_execute(QSqlQuery&);
void query_next(QSqlQuery&);
void error_out(const QString&);

/**
 * Create a select clause with optional where.
 *
 * @param table_name
 * @param field_names
 * @param where_clause
 *
 * @return QString that is a query based on input.
 */
inline QString set_query_statement(const QString& table_name, QString field_names,
                                   const std::optional<QString> where_clause)
{

    // Build the SQL select query
    if (where_clause.value().isEmpty()) {
        return Select % field_names % From % table_name;
    }
    else {
        return Select +
               field_names % From % table_name % Where % where_clause.value();
    }
}

/**
 * Get counts from a table.  Can take a where.
 *
 * @param table_name
 * @param field
 * @param where_clause
 *
 * @return counts of the field;
 */
inline int table_count(const QString& table_name, const QString& field,
                       std::optional<QString>& where_clause)
{
    const auto count_field = "count(" % field % ")";
    QString select_query = set_query_statement(table_name, count_field, where_clause);

    QSqlQuery count {};
    query_prepare(count, select_query);
    query_execute(count);
    query_next(count);
    return count.value(0).toInt();
}

/**
 * Simple send back where for race and sex.
 *
 * @param race_in
 * @param sex_in
 *
 * @return Empty if race is empty, else race_in where.
 */
inline QString where_race_sex(const QString& race_in, const QString& sex_in)
{
    if (!race_in.isEmpty() && !sex_in.isEmpty())
        return my_dnd_queries::Field_Race % my_dnd_queries::Equals % "'" % race_in %
               "'" % my_dnd_queries::And % my_dnd_queries::Field_Sex %
               my_dnd_queries::Equals % "'" % sex_in % "' ";
    else {
        qWarning() << "No value passed to where_race";
        return {};
    }
}

/**
 * Simple send back where race and class.
 *
 * @param race_in
 * @param class_in
 *
 * @return Empty if race is empty, else race_in where.
 */
inline QString where_race_class(const QString& race_in, const QString& class_in)
{
    if (!race_in.isEmpty() && !class_in.isEmpty())
        return my_dnd_queries::Field_Race % my_dnd_queries::Equals % "'" % race_in %
               "'" % my_dnd_queries::And % my_dnd_queries::Field_Class %
               my_dnd_queries::Equals % "'" % class_in % "' ";
    else {
        qWarning() << "No value passed to where_race_class";
        return {};
    }
}

/**
 * For a give field, return T value.  Used for one value.
 *
 * @param query
 * @param field
 *
 * @return Value, only one, from the field in the query.
 *
 * @throws If no field.
 *
 * @note Returns ::Field_Not_Found if not the basic 3.
 */
template <typename T>
T get_value_from_query(const QSqlQuery& query, const QString& field)
{
    int field_no = query.record().indexOf(field);
    if (field_no == Field_Not_Found)
        error_out("Field not found on database: " % field);
    if constexpr (std::is_same_v<T, int>) {
        return query.value(field_no).toInt();
    }
    else if constexpr (std::is_same_v<T, QString>) {
        return query.value(field_no).toString();
    }
    else if constexpr (std::is_same_v<T, bool>) {
        return query.value(field_no).toBool();
    }
    else {
        qWarning() << "Unsupported type: " << typeid(T).name();
        return Field_Not_Found;
    }
}

/**
 * Allow to get a random value from a field with a count of max_count.  Expects a
 * where clause, but can be used for a whole table.
 *
 * @param table
 * @param field
 * @param where_clause
 * @param max_count if 0, will get whole table
 *
 * @return A T value randomly selected.
 */
template <typename T>
T get_random_field_value(const QString& table, const QString& field,
                         std::optional<QString>& where_clause, int max_count)
{

    QSqlQuery query {};
    query.setForwardOnly(true);

    QString random_select_query = set_query_statement(table, field, where_clause);
    random_select_query.append(Limit_1_Offset);

    query_prepare(query, random_select_query);

    // Random based on count.
    std::random_device            rd;
    std::uniform_int_distribution dist(0, max_count - 1);
    const int                     offset = dist(rd);
    query.bindValue(Bind_Offset, offset);
    query_execute(query);

    // Get name from query and return.
    query_next(query);
    return get_value_from_query<T>(query, Field_Name);
}
/**
 * With a type T, provides a comma seperated list of all field names in a struct.
 * Uses boost.
 *
 * @return comma seperated of each filed in T
 */
template <typename T> QString generate_field_names()
{
    QString        field_names;
    bool           first = true;
    constexpr auto names = boost::pfr::names_as_array<T>();
    for (const auto& i : names) {
        if (!first) {
            field_names.append(", ");
        }
        else {
            first = false;
        }
        field_names.append(i);
    }

    return field_names;
}

/**
 * Set up a query and return.  Does not do next.
 *
 * @param table_name
 * @param where_clause
 *
 * @return a query.
 *
 * @note Does not do a next.  Either while it or do a next if expecting one row.
 */
template <typename T>
QSqlQuery set_up_query(const QString&               table_name,
                       const std::optional<QString> where_clause)
{
    QString field_names  = generate_field_names<T>();
    QString select_query = set_query_statement(table_name, field_names, where_clause);

    QSqlQuery query;
    query_prepare(query, select_query);
    query_execute(query);
    return query;
}

/**
 * For a type T, populate it.  Does not do containers, for a container while over and
 * call this for each element.
 *
 * @param query
 *
 * @return Type T.
 */
template <typename T> T populate_one_t(const QSqlQuery& query)
{
    using namespace boost::pfr;
    T rv;
    for_each_field(rv, [&query](auto& field, const auto& idx) {
        auto value       = query.value(idx);
        using field_type = std::decay_t<decltype(field)>;
        if constexpr (std::is_same_v<field_type, int>) {
            field = value.toInt();
        }
        else if constexpr (std::is_same_v<field_type, QString>) {
            field = value.toString();
        }
        else if constexpr (std::is_same_v<field_type, bool>) {
            field = value.toBool();
        }
        else {
            qWarning() << "Unsupported field type for column:" << field;
        }
    });
    return rv;
}

/**
 * Generate binds of type T.
 *
 * @return comma separated values.
 */
template <typename T> QString generate_binds()
{
    QString placeholders;

    boost::pfr::for_each_field(T {}, [&](const auto& field, const std::size_t idx) {
        placeholders.append(":" % QString().setNum(idx) % ",");
    });
    // Get rid of trailing comma.
    placeholders.removeAt(placeholders.size() - 1);
    return placeholders;
}

/**
 * For a give type that has format of bind, value create the bind parameters.
 *
 * @param query
 * @param data_in: bind as first (<0>) and value is as second (<1>).
 */
void bind_parameters(QSqlQuery& query, const auto& data_in)
{

    using namespace boost::pfr;
    QString b {};
    for_each_field(data_in, [&b, &query](const auto& field, const std::size_t idx) {
        auto bind  = ":" % b.setNum(idx);
        auto value = QVariant::fromValue(field);
        query.bindValue(bind, value);
    });
}

/**
 * Do insert with a T that has bind, value format.
 *
 * @param table_name
 * @param data_in
 */
template <typename T>
void insert_into_table(const QString& table_name, const auto& data_in)
{
    // Generate field names and placeholders dynamically
    QString field_names  = generate_field_names<T>();
    QString placeholders = generate_binds<T>();

    // Build the SQL insert query
    QString insert_query {Insert_or_Replace % table_name % " (" % field_names +
                          ")" % Values % "(" % placeholders % ")"};

    QSqlQuery query;
    query_prepare(query, insert_query);

    // Bind the struct fields to the query
    bind_parameters(query, data_in);

    // Execute the query
    query_execute(query);
}

/**
 * Fill type T with data from table_name.  Where is optional.
 *
 * @param table_name
 * @param where_clause
 *
 * @return Type T populated with database data.
 *
 * @note Type T member names must match the table_name column names.
 */
template <typename T>
T select_from_table(const QString&                table_name,
                    const std::optional<QString>& where_clause)
{
    QSqlQuery query = set_up_query<T>(table_name, where_clause);
    query_next(query);
    return populate_one_t<T>(query);
}

/**
 * For a Vector of T, fill up with values.
 *
 * @param table_name
 * @param field
 * @param where_clause
 *
 * @return Vector, not does not check if any records are added.
 */
template <typename T>
std::vector<T> select_from_table(const QString& table_name, const QString& field,
                                 const std::optional<QString>& where_clause)
{

    QString select_query = set_query_statement(table_name, field, where_clause);

    QSqlQuery query;
    query_prepare(query, select_query);
    query_execute(query);

    std::vector<T> rv;
    while (query.next()) {
        rv.push_back(get_value_from_query<T>(query, field));
    }
    return rv;
}

/**
 * Fill vector of type T from a table.
 *
 * @param table_name
 * @param where_clause
 *
 * @return a vector of T from the table.
 */
template <typename T>
std::vector<T> select_all_from_table(const QString&                table_name,
                                     const std::optional<QString>& where_clause)
{

    QSqlQuery      query = set_up_query<T>(table_name, where_clause);
    std::vector<T> rv;
    while (query.next()) {
        rv.push_back(populate_one_t<T>(query));
    }

    return rv;
}

/**
 * Create a where on one field with one value.
 *
 * @param where_field
 * @param where_value
 * @param not_where
 *
 * @return Where statement.  Can be null or not value for where_value;
 *
 * @todo Implement multiple wheres.
 */
template <typename T>
QString simple_where(const QString& where_field, const T where_value,
                     const bool not_where = false)
{
    if (where_field.isEmpty())
        return {};
    QString what {};
    if constexpr (std::is_same_v<T, int>) {
        what = QString().setNum(where_value);
    }
    else if constexpr (std::is_same_v<T, QString>) {
        if (!where_field.isEmpty())
            what = "'" % where_value % "'";
    }
    else if constexpr (std::is_same_v<T, bool>) {
        what = QString().setNum(where_value);
    }
    if (!not_where)
        return where_field % Equals % what;
    else
        return where_field % NotEquals % what;
}
}    // namespace my_dnd_queries

#endif    // MY_DND_QUERIES_H

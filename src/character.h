#ifndef MYDND_CHARACTER_CHARACTER_H
#define MYDND_CHARACTER_CHARACTER_H

#include "class.h"
#include "db_ops.h"
#include "my_dnd.h"
#include "my_dnd_constants.h"
#include "my_dnd_enums.h"
#include "race.h"

#include <QDebug>
#include <QString>

namespace mydnd_character {

/** @file character.h
 *  @brief Provides general classes for a character
 *  @page page1_MYDND My DnD character classes
 *  @tableofcontents
 *  @section Overview_MYDND Overview
 *  Provides classes for attributes of characters, spells, monsters, and so on.
 *  ## Player Characters (aka: RCC)
 *  - There is @a Race, @a Class, and @a Character as the base name (not base class).
 *      - Each class relies on attributes
 *          - @ref my_dnd::Race_attributes
 *          - @ref my_dnd::Class_attributes
 *          - @ref my_dnd::Character_attributes
 *  - @a information hold attributes of each base name.
 *      - For example @ref Race_information has @ref my_dnd::Race_attributes
 *      attributes.
 *  - @a addition_info and @a info holds information that is not part of playing, but
 *  more for character creation.  info is short as a clue that not the information is
 *  not part of playing.
 *  - @a standard holds a vector of associated base attribute class for non-Character
 *  classes.
 *  - @a Playable_characters is the equivalent of standard.
 *  - There is a a @a misc_info, these are under Character and are not tied to a class
 *  or race.
 */

////////////////////////////////////////////////////////////////////////////////
/// usings
////////////////////////////////////////////////////////////////////////////////
using my_dnd::Attrib_value_type;

////////////////////////////////////////////////////////////////////////////////
/// forwards
////////////////////////////////////////////////////////////////////////////////

struct Ability_type;
class Race_information;
class Race_additional_information;
class Race_additional_standard;
class Class_information;
class Character_information;
class Character_misc_standard;

bool check_modifier(const Ability_type&);
bool check_ability(const Ability_type&);

int     calculate_number_new_skills(const Character_information&);
int     calculate_number_new_feats(const Character_information&);
int     calculate_next_level(int);
int     calculate_bonus_spell(int, int);
int     get_bonus_spell(const QString&, const Ability_type&);
QString set_character_qpix(const QString&, const QString&);

my_dnd::attribute_type roll_ability_score();

void calculate_vital_statistics(Character_information&);

////////////////////////////////////////////////////////////////////////////////
/// Classes
////////////////////////////////////////////////////////////////////////////////

/**
 * Contains a player characters data.  For race and class, the race and class are
 * listed, but must be matched back to @ref Race_information and @ref
 * Class_information.  The same goes for what spells, and equipment the character
 * will have.  The default constructor is used to populate from a complete
 * character, the new is used when creating a new character; this new populates
 * abilities with random numbers (4d6).  As an information class, the gets and
 * sets are similar.
 *
 * @todo Other stuff like equipment, spells, list of campaigns, deaths:)
 * @todo How to do a saved character--maybe in new
 * @todo A lot member functions that are helpers.  Need to move out.
 * @todo For default constructor--need to check character is valid.  Use
 * get_new_character_flag flag.
 */
class Character_information {
public:
    explicit Character_information();
    void make_new_character();
    void load_a_character(const QString& uuid);
    // Gets
    Attrib_value_type get_attribute(const my_dnd::Character_attributes&) const;
    Attrib_value_type get_attribute(const my_dnd::Class_attributes& whats) const
    {
        return class_info.get_class_attribute(whats);
    }
    Attrib_value_type get_attribute(const my_dnd::Race_attributes& whats) const
    {
        return race_info.get_race_attribute(whats);
    }
    QString get_character_name() const    /// Simple, but can use the regular get.
    {
        return character_multi_pass.name.as_string;
    }
    QString get_character_id() const
    {
        return character_multi_pass.id.as_string;
    }
    QString get_sex() const
    {
        return sex.as_string;
    }
    QString get_race_name() const    /// Keep simple, but can use the regular get.
    {
        return race.as_string;
    }
    int get_race_index() const
    {
        return race.as_int;
    }
    QString get_class_type_name() const    /// Keep simple, can use get_
    {
        return char_class.as_string;
    }
    Ability_type get_abilities() const
    {
        return abilities;
    }

    Ability_modifiers get_ability_modifers() const
    {
        return ability_modifiers;
    }
    bool get_new_character_flag() const    ///\todo needed?
    {
        return new_character;
    }
    my_dnd::Character_name_type get_multipass() const
    {
        return character_multi_pass;
    }
    my_dnd::Armor_class_data get_armor_class() const
    {
        return armor_class;
    }
    QString get_pic() const
    {
        // QString temp {set_character_qpix(race.as_string, gender.as_string)};
        return qpix.as_string;    // qpix.as_string;
    }

    std::vector<Class_skills_information> get_skills() const
    {
        return skills;
    }
    /// TODO: testing
    Damage_reduction     dam_reduct;
    std::vector<QString> class_features;

    // Sets
    void set_attribute(const QVariant&, const my_dnd::Character_attributes&);

    void roll_new_character();
    void update_modifiers();
    void set_character_name(const QString& name_in)
    {
        character_multi_pass.name.set_avt(name_in);
    }
    void set_character_id(const QString& id_in)
    {
        character_multi_pass.id.set_avt(id_in);
    }
    void update_new_character_flag(bool new_character_flag)
    {
        new_character = new_character_flag;
    }
    void set_class_data(const Class_information& class_data_in)
    {
        class_info = class_data_in;
    }
    void set_race_data(const Race_information& race_data_in)
    {
        race_info = race_data_in;
    }
    void set_skills(const std::vector<Class_skills_information>& skill_in)
    {
        skills = skill_in;
    }

    void save_character();

private:
    void update_race_info();
    void set_armor_class();

    // Race and class for character (saves long calls and not much memory used)
    Race_information  race_info;
    Class_information class_info;
    // Player values
    Ability_type      abilities;    // Abilities with Race adds
    Ability_modifiers ability_modifiers;

    my_dnd::Character_name_type           character_multi_pass {};
    Attrib_value_type                     level {};
    Attrib_value_type                     xp {};
    Attrib_value_type                     qpix;
    std::vector<Class_skills_information> skills;

    // TODO: Why Set to blanks, not the default.
    Attrib_value_type race       = {.as_string = "", .as_int = 0};
    Attrib_value_type char_class = {.as_string = "", .as_int = 0};
    Attrib_value_type deity      = {.as_string = "", .as_int = 0};
    Attrib_value_type sex        = {.as_string = "", .as_int = 0};
    Attrib_value_type alignment  = {.as_string = "", .as_int = 0};
    Attrib_value_type age        = {.as_string = "0", .as_int = 0};
    Attrib_value_type height     = {.as_string = "0", .as_int = 0};
    Attrib_value_type weight     = {.as_string = "0", .as_int = 0};
    Attrib_value_type eye_color  = {.as_string = "", .as_int = 0};
    Attrib_value_type skin_color = {.as_string = "", .as_int = 0};

    // Combat gear and such
    my_dnd::Armor_class_data armor_class;

    // equipped
    Attrib_value_type equipped_shield;
    Attrib_value_type equipped_armor;
    bool new_character {false};    /// Unless explicit, not a new character
};

///
/// Vector of all playable characters.
/// Call with [] to get a character based on multi_pass id.
///
class Player_characters {
public:
    explicit Player_characters();

    Character_information& operator[](const QString&);

    std::vector<my_dnd::Character_name_type> get_character_name_list()
    {
        return playable_characters_list;
    }

    void read_characters()
    {
        // read_in_character(playable_characters);
    }
    void add_character(const Character_information&);
    void write_characters()
    {
        //        write(playable_characters);
    }

private:
    std::vector<Character_information>       playable_characters;
    std::vector<my_dnd::Character_name_type> playable_characters_list;
};

/**
 * For attributes that are generated during character creation.
 *
 * @note no information class as not tied to a race or class.
 *
 * @todo All misc filled.
 */
class Character_misc_standard {
public:
    explicit Character_misc_standard();

    // gets
    std::vector<Attrib_value_type> get_character_misc_information(
        const my_dnd::Character_misc_attributes& which_to_get) const;
    std::vector<Attrib_value_type> get_character_misc_information(
        const my_dnd::Character_misc_attributes& which_to_get, const QString class_in,
        const QString race_in);

private:
    std::vector<Attrib_value_type> alignments;
    std::vector<Attrib_value_type> sexes;
    std::vector<Attrib_value_type> eye_colors;
    std::vector<Attrib_value_type> hair_colors;
    std::vector<Attrib_value_type> skin_colors;
    std::vector<Attrib_value_type> deity;
};

}    // namespace mydnd_character

#endif

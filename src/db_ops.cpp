/**
 * @file
 *
 *  All database operations: class set to open and close, functions to access,
 *  functions to write to.
 *
 *  Use of std::vector, but QString and such in the vector.
 *
 *  Except for opening database, no direct functions.  Use queries.h.
 *
 */

#include "db_ops.h"

#include "queries.h"    // IWYU pragma: keep

namespace my_dnd {

/**
 * Get starting height and weight.
 *
 * @param race
 * @param sex
 *
 * @return Rolled height and weight.
 *
 * @note if sex is Neutral--all 0;
 */
Rolled_height_weight get_starting_height_weight(const QString& race,
                                                const QString& sex)
{
    Rolled_height_weight rv;
    // Neutral is not implemented, so return with defaults.
    // If others empty, return defaults.
    if (sex.contains(Neutral_Filter) || race.isEmpty() || sex.isEmpty()) {
        return rv;
    }

    using namespace my_dnd_queries;

    auto data_in = select_from_table<Starting_height_weight_data>(
        Table_Starting_Height_Weight, where_race_sex(race, sex));

    Dice_roll_info roll {.rolls = data_in.height_rolls, .sides = data_in.height_die};
    const int      temp_roll = roll_die(roll);
    rv.height                = data_in.base_height + temp_roll;

    roll.rolls = data_in.weight_rolls;
    roll.sides = data_in.weight_die;
    rv.weight  = data_in.base_weight + temp_roll * roll_die(roll);

    return rv;
}

/**
 * Returns all alignments in an array.  For player character limited, pass
 * character_only as true.
 *
 * @param character_only Set to true to get only player character (not evil).
 *
 * @return Array of alignments.
 */
std::vector<QString> get_alignments(const bool character_only = false)
{
    QString where = my_dnd_queries::simple_where<bool>(
        my_dnd_queries::Field_Character_Alignment, character_only);
    auto rv = my_dnd_queries::select_from_table<QString>(
        my_dnd_queries::Table_Alignments, my_dnd_queries::Field_Alignment, where);
    return rv;
}

/**
 * Get alignments based on class.
 *
 * @param class_in
 *
 * @return Array of alignments for a class.
 */
std::vector<QString> get_alignments(const QString& class_in)
{
    QString where =
        my_dnd_queries::simple_where(my_dnd_queries::Field_Class, class_in);
    auto rv = my_dnd_queries::select_from_table<QString>(
        my_dnd_queries::Table_Allowed_Alignments, my_dnd_queries::Field_Alignment,
        where);

    return rv;
}
/**
 * Set player character alignments in an array.
 *
 * @return Player Character only alignments.
 */
std::vector<QString> get_character_alignments()
{
    return get_alignments(true);
}

/**
 * Get all sexes into an array.
 */
std::vector<QString> get_all_sex_query()
{
    QString where =
        my_dnd_queries::simple_where(my_dnd_queries::Field_Sex, Neutral_Filter, true);
    auto rv = my_dnd_queries::select_from_table<QString>(
        my_dnd_queries::Table_Sex, my_dnd_queries::Field_Sex, where);

    return rv;
}

/**
 * With new characters, get the base traits for a class.
 *
 * @param class_in
 *
 * @return Struct of base class
 */
Base_class_traits get_base_class_traits(const QString& class_in)
{
    return my_dnd_queries::select_from_table<Base_class_traits>(
        my_dnd_queries::Table_Base_Class_Traits,
        my_dnd_queries::simple_where<QString>(my_dnd_queries::Field_Class, class_in));
}

/**
 * With new characters, get the base traits for a race.
 *
 * @param race_in
 *
 * @return Struct of base race traits.
 */
Base_race_traits get_base_race_traits(const QString& race_in)
{

    return my_dnd_queries::select_from_table<Base_race_traits>(
        my_dnd_queries::Table_Base_Race_Traits,
        my_dnd_queries::simple_where<QString>(my_dnd_queries::Field_Race, race_in));
}

/**
 * Get all races.
 *
 * @return All races available for a player.
 */
std::vector<QString> get_races()
{
    auto rv = my_dnd_queries::select_from_table<QString>(
        my_dnd_queries::Table_Races, my_dnd_queries::Field_Race, QString());
    return rv;
}

/**
 * Get all classes.
 *
 * @return All classes available for a player.
 */
std::vector<QString> get_classes()
{
    auto rv = my_dnd_queries::select_from_table<QString>(
        my_dnd_queries::Table_Classes, my_dnd_queries::Field_Class, QString());
    return rv;
}

/**
 * Get all weapons.
 *
 * @return Vector of all weapons data in database.
 */
std::vector<Weapons_database_data> get_weapons()
{
    auto rv = my_dnd_queries::select_all_from_table<Weapons_database_data>(
        my_dnd_queries::Table_Weapons, no_where_statement);

    return rv;
}

std::vector<Monster_database_data> get_monster_data()
{
    return my_dnd_queries::select_all_from_table<Monster_database_data>(
        my_dnd_queries::Table_Monsters, no_where_statement);
}

/**
 * Clerics are limited on deities, pass in the race to filter those.
 *
 * @param race
 *
 * @return Vector of race appropriate deities for a cleric.
 */
std::vector<QString> get_cleric_deities(const QString& race)
{
    using namespace my_dnd_queries;
    auto where = my_dnd_queries::simple_where(Field_Race, race);
    auto rv    = my_dnd_queries::select_from_table<QString>(
        my_dnd_queries::Table_Allowed_Deity, my_dnd_queries::Field_Deity, where);
    return rv;
}

/**
 * Initial value of deities for a player.
 *
 * @return All deities from the @ref my_dnd_queries::Table_Deities.
 */
std::vector<QString> get_deities()
{
    auto rv = my_dnd_queries::select_from_table<QString>(
        my_dnd_queries::Table_Deities, my_dnd_queries::Field_Deity, QString());
    return rv;
}

/**
 * Get starting age for a character.
 *
 * @param race
 * @param char_class
 *
 * @return The age of a new character.
 */
int get_starting_age(const QString& race, const QString& char_class)
{
    auto age {0};
    if (race.isEmpty() || char_class.isEmpty()) {
        return age;
    }
    using namespace my_dnd_queries;

    QString where    = where_race_class(race, char_class);
    auto    age_data = select_from_table<Age_calculation_data>(
        Table_Starting_Age, where_race_class(race, char_class));
    const Dice_roll_info roll {age_data.rolls, age_data.die};
    age = age_data.adulthood + roll_die(roll);
    return age;
}

/**
 * Get a randon name for a race and sex.
 *
 * @param race
 * @param sex
 *
 * @return A valid random character name.
 */
QString get_rand_name(const QString& race, const QString& sex)
{
    using namespace my_dnd_queries;

    // Get count as needed to filter rest.
    std::optional<QString> where = where_race_sex(race, sex);

    const int range {table_count(Table_Random_Names, Field_Name, where)};

    return get_random_field_value<QString>(Table_Random_Names, Field_Name, where,
                                           range);
}

/**
 * Get the various race descriptions for race_in.
 *
 * @param race_in
 *
 * @return All types of descriptions for race.
 */
Race_descriptions get_race_descriptions(const QString& race_in)
{
    return my_dnd_queries::select_from_table<Race_descriptions>(
        my_dnd_queries::Table_Race_Descriptions,
        my_dnd_queries::simple_where<QString>(my_dnd_queries::Field_Race, race_in));
}

/**
 * Get various class descriptions for class_in.
 *
 * @param class_in
 *
 * @return All types of descriptions for a characters class.
 */
Class_descriptions get_class_descriptions(const QString& class_in)
{
    Class_descriptions temp;
    return my_dnd_queries::select_from_table<Class_descriptions>(
        my_dnd_queries::Table_Class_Descriptions,
        my_dnd_queries::simple_where<QString>(my_dnd_queries::Field_Class, class_in));
}

/**
 * New character data only.  That is, data specific to a new character that is not
 * use once a character is fully developed.
 *
 * @param rand_sorted
 * @param index_in
 * @param uuid
 *
 */
void get_new_char(mydnd_character::Ability_type& rand_sorted,
                  mydnd_character::Ability_type& index_in, const QString uuid)
{
    auto data_in = my_dnd_queries::select_from_table<New_character_database_data>(
        my_dnd_queries::Table_New_Characters,
        my_dnd_queries::simple_where<QString>(my_dnd_queries::Field_Uuid, uuid));
    index_in.set_ability_negative(my_dnd::Str, data_in.str_index);
    index_in.set_ability_negative(my_dnd::Dex, data_in.dex_index);
    index_in.set_ability_negative(my_dnd::Con, data_in.con_index);
    index_in.set_ability_negative(my_dnd::Int, data_in.int_index);
    index_in.set_ability_negative(my_dnd::Wis, data_in.wis_index);
    index_in.set_ability_negative(my_dnd::Cha, data_in.cha_index);

    rand_sorted.set_ability_negative(my_dnd::Roll_1, data_in.roll_1);
    rand_sorted.set_ability_negative(my_dnd::Roll_2, data_in.roll_2);
    rand_sorted.set_ability_negative(my_dnd::Roll_3, data_in.roll_3);
    rand_sorted.set_ability_negative(my_dnd::Roll_4, data_in.roll_4);
    rand_sorted.set_ability_negative(my_dnd::Roll_5, data_in.roll_5);
    rand_sorted.set_ability_negative(my_dnd::Roll_6, data_in.roll_6);
}

/**
 * Base traits.  Equipment, list of other will be in other data and saves.
 *
 * @param uuid
 */
Character_database_data get_character(const QString& uuid)
{
    using namespace my_dnd_queries;
    using namespace mydnd_character;
    auto rv = select_from_table<Character_database_data>(
        my_dnd_queries::Table_Characters, " uuid = '" % uuid % "'");

    return rv;
}

// Saves and Writes

/**
 * Save data to the new character table.
 *
 * @param abilities
 * @param indexes
 * @param uuid
 */
void save_new_char(const mydnd_character::Ability_type& abilities,
                   const mydnd_character::Ability_type& indexes, const QString& uuid)
{
    New_character_database_data temp {};
    temp.uuid      = uuid;
    temp.roll_1    = abilities[my_dnd::Roll_1].as_int;
    temp.roll_2    = abilities[my_dnd::Roll_2].as_int;
    temp.roll_3    = abilities[my_dnd::Roll_3].as_int;
    temp.roll_4    = abilities[my_dnd::Roll_4].as_int;
    temp.roll_5    = abilities[my_dnd::Roll_5].as_int;
    temp.roll_6    = abilities[my_dnd::Roll_6].as_int;
    temp.str_index = indexes[my_dnd::Str].as_int;
    temp.dex_index = indexes[my_dnd::Dex].as_int;
    temp.con_index = indexes[my_dnd::Con].as_int;
    temp.int_index = indexes[my_dnd::Int].as_int;
    temp.wis_index = indexes[my_dnd::Wis].as_int;
    temp.cha_index = indexes[my_dnd::Cha].as_int;

    using namespace my_dnd_queries;
    insert_into_table<New_character_database_data>(Table_New_Characters, temp);
}
/**
 * Save base traits for a character.
 *
 * @param data_in
 */
void save_a_character(const Character_database_data& data_in)
{
    using namespace my_dnd_queries;

    QString table = Table_Characters;
    insert_into_table<Character_database_data>(table, data_in);
}

/**
 * Open the database at path.
 *
 * @param path
 *
 * @exception db Throws if already open or cannot open.
 */
void DB_ops::open_db(const QString& path)
{
    Character_database_data data_in;
    db_file = path;
    if (db.isOpen())
        mydnd_err::error("DB is open!");
    db.setDatabaseName(db_file);
    if (!db.open()) {
        const QString error_message =
            "Could not open database: " + db.lastError().text();
        mydnd_err::error(error_message.toStdString());
    }
}

}    // namespace my_dnd

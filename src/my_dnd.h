/**
 *@file
 **/
#ifndef MY_DND_MY_DND_H
#define MY_DND_MY_DND_H

#include "../gui/error_and_debug.h"
#include "my_dnd_constants.h"
#include "my_dnd_enums.h"
#include "shared_types.h"

#include <QString>
#include <algorithm>
#include <iterator>
#include <typeinfo>
#include <utility>
#include <vector>

namespace my_dnd {

/**
 * @name HACKS
 * @anchor HACKS
 *@{
 **/
#ifdef _WIN32
inline const QString file_loc {"C:/Users/bikep/source/repos/my_dnd/data/"};
#else
inline const auto file_loc = QStringLiteral("/home/mbenton/projects/my_dnd/data/");
#endif
/// END HACKS
///@}
///

////////////////////////////////////////////////////////////////////////////////
///  Forward declarations and such
////////////////////////////////////////////////////////////////////////////////

/**
 * Simple implementation of is_even.
 *
 * @param  value is an int
 *
 * @return  true=even, false=odd
 *
 * @warning does not throw
 **/
inline bool is_even(int value) noexcept
{
    return (value % 2 == 0);
}

// Using
using ability_score_type   = die_type;    ///< Scores and such go back to die types
using ability_score_string = QString;     ///< We are using QString

// Class and structs forward declarations
struct Attrib_value_type;

// Function forward declarations
Attrib_value_type calculate_ability_modifier(Attrib_value_type);
int               minimum_itl_check(int);

////////////////////////////////////////////////////////////////////////////////
///  Structures and Classes
////////////////////////////////////////////////////////////////////////////////

///
/// Storage for character multi-pass (name and id)
///
struct Character_name_type {
    Attrib_value_type name;
    Attrib_value_type id;    /// id is 10 upper alpha and 10 numbers (leading 0s).
    friend bool operator<(const Character_name_type& l, const Character_name_type& r)
    {
        return l.name.as_string < l.name.as_string;
    }
};

///
/// Weapon familiarity for classes.
///
struct Weapon_familiarity {
    Weapon_familiarity(const QString& w, const QString& f):
        weapon {w}, familiarity {f}
    {
    }
    QString weapon;
    QString familiarity;
};

///
/// For bonus fighting certain creature or creature types.
///
struct Creature_bonus {
    Creature_bonus(const QString& c, const int b): creature {c}, bonus {b}
    {
    }
    QString creature;
    int     bonus {0};
};

struct Armor_class_data {
    Attrib_value_type base_ac = {.as_string = QString::number(base_armor_class),
                                 .as_int    = base_armor_class};
    Attrib_value_type armor_bonus;
    Attrib_value_type shield_bonus;
    Attrib_value_type dexterity_modifer;
    Attrib_value_type size_modifer;
    Attrib_value_type armor_dexterity_max;
    Attrib_value_type carrying_load_max;
    Attrib_value_type total_ac;
};

}    // namespace my_dnd

#endif

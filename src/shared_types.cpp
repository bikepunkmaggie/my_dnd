//
// Created by mbenton on 12/12/24.
//

#include "shared_types.h"

#include "../gui/error_and_debug.h"

#include <QDebug>
#include <QStringBuilder>
namespace my_dnd {

static std::random_device rd;    ///< Using hardware if can!

/**
 * Validate number of sides.
 *
 * @param sides
 *
 * @exception sides If less than min or max, will throw.
 */
inline void validate_sides(die_type sides)
{
    // Just the common sense check.
    if (sides <= max_die_size && sides >= min_die_size)
        return;

    mydnd_err::error("Invalid die sides: ", sides);
}

/**
 * Validate the number of rolls.
 *
 * @param rolls
 *
 * @throw rolls if less than min_rolls or greater than max_rolls
 */
inline void validate_rolls(die_type rolls)
{
    // Just the commoner sense check.
    if (rolls >= min_rolls && rolls <= max_rolls)
        return;
    mydnd_err::error("Invalid rolls: ", rolls);
}

/**
 * Roll a die and return results.
 *
 * @param roll_in type of roll.  @ref Dice_roll_info
 *
 * @return die_type of the roll
 *
 * @note No negative returns.
 * @note Bound checks for @ref Dice_roll_info done here.
 *
 * @throw roll_in not valid (w/in range and populated)
 */
die_type roll_die(const Dice_roll_info& roll_in)
{

    //  Check valid roll_in from size to having d
    const auto number_rolls = roll_in.rolls;
    const auto die_sides {roll_in.sides};
    validate_sides(die_sides);
    validate_rolls(number_rolls);

    std::uniform_int_distribution<int> dist(default_die_start, roll_in.sides);

    auto roll_results {0};
    for (auto i = 0; i < roll_in.rolls; ++i) {
        roll_results += dist(rd);
    }
    return roll_results;
}

/**
 * Call simply to get a uuid, when a unique id is needed.
 * All player characters, all NPCs once in the game.
 *
 * @return a QString of a valid character id.
 */
QString get_uuid()
{
    return QUuid::createUuid().toString();
}

/** Rolls and removes the lowest roll.
 *
 * @param roll_in
 *
 * @returns the sum w/o the lowest roll.
 *
 * @throw Checks are done if not a valid roll_in, will error.
 */
die_type roll_drop_lowest(const Dice_roll_info& roll_in)
{

    auto sides = roll_in.sides;
    auto rolls = roll_in.rolls;

    validate_rolls(rolls);
    validate_sides(sides);

    // This will be overridden once a lower value is rolled.
    auto min_value {sides};
    auto sum_rolls {0};

    // For each roll, do a new roll.  If teh roll is less than current min, replace
    // that min.  Keep a sum that is the number of rolls.  Once done, just subtract
    // the minimum value.
    for (auto i = 0; i < rolls; ++i) {
        Dice_roll_info temp {.rolls = 1, .sides = sides};
        auto           x {roll_die(temp)};
        sum_rolls += x;
        if (x < min_value)
            min_value = x;
    }
    return sum_rolls - min_value;
}

}    // namespace my_dnd
namespace mydnd_character {

Ability_type::Ability_type()
{
    reset_ability_vector();
};
void Ability_type::clear_data()
{
    for (auto& i : abilities)
        i.set_avt(0);
};

/**
 * Set all to 0.
 */
void Ability_type::reset_ability_vector()
{
    abilities.clear();
    abilities.resize(my_dnd::num_abilities);
}

// Overload operator[] for getter
const my_dnd::Attrib_value_type& Ability_type::operator[](size_t index) const
{
    if (index >= my_dnd::num_abilities) {
        throw std::out_of_range("Ability index out of range");
    }
    return abilities[index];
}
void Ability_type::set_ability(int ability, int value)
{
    if (value < 1)
        value = my_dnd::as_int_default;
    if (ability <= my_dnd::Cha && ability >= my_dnd::Str)
        abilities[ability].set_avt(value);
}
void Ability_type::set_ability(int ability, QString value)
{
    if (value.isEmpty())
        return;
    if (ability <= my_dnd::Cha && ability >= my_dnd::Str)
        abilities[ability].set_avt(value);
}
void Ability_type::set_ability_negative(int ability, int value)
{
    if (ability <= my_dnd::Cha && ability >= my_dnd::Str)
        abilities[ability].set_avt(value);
}
int Ability_type::size(void)
{
    return abilities.size();
}

///
/// Check if max abilities is not below low_ability and modifier it good.
/// All abilities must have one ability above \ref my_dnd::low_ability.
/// \param abilities_in
/// \return true at least one is above
/// \ref my_dnd::low_ability.
///
bool Ability_type::check_ability(const Ability_type& abilities_in)
{
    auto max_ability_value {abilities_in.max()};

    //        if (!check_modifier(abilities_in) ||
    //            (max_ability_value <= my_dnd::low_ability))
    //            return false;

    return true;
}
int Ability_type::max() const
{
    if (abilities.empty())
        return 0;
    return std::max_element(
               abilities.begin(), abilities.end(),
               [](const auto& a, const auto& b) { return a.as_int < b.as_int; })
        ->as_int;
}

/**
 * Rolled abilities for new characters.  Sorted once rolled.
 * Rolls when created.
 */
Rolled_abilities::Rolled_abilities()
{
    reroll();
}
void Rolled_abilities::reroll()
{
    std::vector<int>       temp;
    my_dnd::Dice_roll_info t {.rolls = my_dnd::ability_rolls,
                              .sides = my_dnd::default_die_side};
    temp.reserve(my_dnd::num_abilities);
    for (auto i = 0; i < my_dnd::num_abilities; i++) {
        temp.push_back(roll_ability_score(t));
    }
    std::ranges::sort(temp);
    reset_ability_vector();
    for (auto i = 0; i < my_dnd::num_abilities; i++) {
        set_ability(i, temp[i]);

        auto s = "Roll " % QString::number(i + 1) % ": " % QString::number(temp[i]);
        set_ability(i, s);
    }
}

///
/// Populates with 4d6 (or what ever set too).
/// \returns \ref my_dnd::die_type with an ability roll
///
my_dnd::die_type Rolled_abilities::roll_ability_score(
    const my_dnd::Dice_roll_info& roll_in)
{

    return my_dnd::roll_drop_lowest(roll_in);
}

bool Ability_modifiers::check_min_modifer(void)
{
    if (is_empty())
        return false;
    int c = 0;
    for (auto i = 0; i < size(); i++)
        c += get_ability(i);
    return c > my_dnd::low_mods ? true : false;
};

void Ability_modifiers::set_ability_modifier(Ability_type data_in)
{
    if (data_in.is_empty())
        return;
    for (auto i = 0; i < data_in.size(); i++) {
        auto value = data_in[i].as_int;
        if (value < 1)
            value = my_dnd::as_int_default;
        //        abilities[i].set_avt(calculate_ability_modifier(value));
        set_ability(i, calculate_ability_modifier(value));
    }
}

// Don't need.
/**
 * Calculate Modifier, with even/odd as one value have a bit of math.
 *
 * @param ability
 *
 * @return modified
 *
 * @todo Need test and checks
 */
int Ability_modifiers::calculate_ability_modifier(int ability)
{

    if (ability % 2)
        ability--;
    return ability / 2 - my_dnd::mod_adjust;
}
}    // namespace mydnd_character

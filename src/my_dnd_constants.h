/**
 * @file      my_dnd_constants.h
 * @author    Mike H Benton (bikepunk005@hotmail.com)
 * @brief     Namespace constants
 * @version   0.1
 * @date      2022-01-28
 * @copyright Copyright (c) 2024
 */

#ifndef MY_DND_MY_DND_CONSTANTS
#define MY_DND_MY_DND_CONSTANTS
#include <QString>

namespace my_dnd {
/** Level information */
constexpr auto start_level {1};
constexpr auto experience_multipler {1000};

//  Misc constants.
/// General not found flag
constexpr auto not_found {-1};

/// The lowest ability that a character can have.
///  Details on Page 8, re-rolling.
constexpr auto low_ability {13};

/// For bonus spells:
/// (spell_level*multiplier)-offset, use to get mod adjustment.  Then divide
/// by 4.
constexpr auto bonus_spell_multiplier {2};
constexpr auto bonus_spell_offset {3};
constexpr auto bonus_spell_numerator {4.0};

// multipler
/// Skill constants
constexpr auto skills_start_multiplier {4};
constexpr auto skills_levelup_bonus {1};

/// Minimum intelligence for player characters
/// See Table 2-1
constexpr auto minimum_intelligence {3};

/** @name Spell Character Info
 *  @{
 */
const inline QString spell_itl_bonus_class {"Wizard"};
const inline QString spell_wis_bonus_class {"Cleric Druid Paladin Ranger"};
const inline QString spell_cha_bonus_class {"Sorcerer Bard"};
/** @}*/

////////////////////////////////////////////////////////////////////////////////
/// Combat
////////////////////////////////////////////////////////////////////////////////
constexpr auto base_armor_class {10};
}    // namespace my_dnd
#endif

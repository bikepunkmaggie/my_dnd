//
// Created by mbenton on 11/22/24.
//

#ifndef MYDND_SRC_RACE_H
#define MYDND_SRC_RACE_H
#include "db_ops.h"
#include "my_dnd.h"
#include "shared_types.h"

namespace mydnd_character {
using my_dnd::Attrib_value_type;
/**
 *  The Information classes contain all attributes specific to that type of race.
 *  All race attributes for a race are contained here.  For all player characters:
 *  - @ref my_dnd::Race_attributes
 */
class Race_information {
public:
    explicit Race_information();
    explicit Race_information(const QString&);
    // Gets
    Attrib_value_type get_race_attribute(const my_dnd::Race_attributes&) const;
    Attrib_value_type get_vec_attribute(const my_dnd::Race_attributes&) const;
    /// Keep simple, but can use the regular get.
    [[nodiscard]] QString get_race_name() const
    {
        return race.as_string;
    }

    // Sets
    void set_attribute(const QVariant&, const my_dnd::Race_attributes&);
    void set_race_name(const QString& race_name)    /// Set here, then populate.
    {
        race.set_avt(race_name);
    }

private:
    /// \name Race_attributes_variables
    ///@{
    my_dnd::Race_descriptions descriptions;
    Attrib_value_type         race;         ///< Match up with Character_races
    Attrib_value_type         favored;      ///< Favored class
    Attrib_value_type         addl_lang;    ///< Additional languages
    Attrib_value_type         creat_size;
    Attrib_value_type         str_adj;
    Attrib_value_type         dex_adj;
    Attrib_value_type         con_adj;
    Attrib_value_type         itl_adj;
    Attrib_value_type         wis_adj;
    Attrib_value_type         cha_adj;
    Attrib_value_type         land_speed;
    Attrib_value_type         age;
    Attrib_value_type         extra_feat;
    Attrib_value_type         no_armor_speed_reduction;
    Attrib_value_type         darkvision;
    Attrib_value_type         stonecunning;
    Attrib_value_type         low_light_vision;
    Attrib_value_type         small_size_bonus;
    Attrib_value_type         giant_bonus;
    Attrib_value_type         stability_bonus;
    Attrib_value_type         personality;
    ///@}
};

/**
 * Vector of race information.
 * Call with [] to get a race.
 * Use @ref get_race_list to have a vector of race
 * Use @ref get_race_data for a vector of all @ref Race_information
 */
class Race_standard {
public:
    explicit Race_standard();

    Race_information& operator[](const QString&);

    std::vector<QString> get_race_list() const
    {
        return race_list;
    }

    std::vector<Race_information> get_race_data() const
    {
        return race_data;
    }

private:
    std::vector<Race_information> race_data;    ///< Vector of all race data.
    std::vector<QString>          race_list;    ///< Each race for selectors
};

/**
 * The Information classes contain all attributes specific to that type of race.
 * All additional race attributes for a race are contained here. Generally, used
 * during character creation.
 */
class Race_additional_information {
public:
    explicit Race_additional_information();
    // Gets
    Attrib_value_type get_race_attribute(const my_dnd::Race_misc_attributes&) const;
    QString get_race_name() const    /// Keep simple, but can use the regular get.
    {
        return race.as_string;
    }

    // Sets
    void set_race_name(const QString& race_name)    /// Set here, then populate.
    {
        race.set_avt(race_name);
    }

private:
    Attrib_value_type race;
    Attrib_value_type base_height;
    Attrib_value_type base_weight;
    Attrib_value_type height_mod;
    Attrib_value_type weight_mod;
    Attrib_value_type male_height_mod;
    Attrib_value_type male_weight_mod;
};

;

}    // namespace mydnd_character

#endif    // MYDND_SRC_RACE_H

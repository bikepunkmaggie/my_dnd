//
// Created by mbenton on 11/22/24.
//

#ifndef MYDND_SRC_CLASS_H
#define MYDND_SRC_CLASS_H

#include "class_features.h"
#include "db_ops.h"
#include "my_dnd.h"
#include "shared_types.h"

namespace mydnd_character {

using my_dnd::Attrib_value_type;
struct Class_skills_information {
    Attrib_value_type skill;
    Attrib_value_type key_ability;
    Attrib_value_type untrained_flag;
};
///
/// The Information classes contain all attributes specific to that type of class.
/// All class attributes are contained here.  For all player characters:
///
class Class_information {
public:
    explicit Class_information();
    explicit Class_information(const QString&);
    // Gets
    Attrib_value_type get_class_attribute(const my_dnd::Class_attributes&) const;

    QString get_class_type_name() const
    {
        return class_type.as_string;    /// Use to get class.
    }

    std::vector<Feature_value_type> get_features()
    {
        return features;
    }
    // Sets
    void set_class_type_name(const QString& class_name)
    {
        class_type.set_avt(class_name);    // Use to set class.
    }

private:
    Attrib_value_type class_type;
    Attrib_value_type hit_die;
    Attrib_value_type gold_rolls;
    Attrib_value_type alingment_restriction;
    Attrib_value_type class_type_description;
    Attrib_value_type class_type_brief;
    Attrib_value_type diety_restrictions;
    Attrib_value_type damage_reduction;
    Attrib_value_type base_skill_points;
    Attrib_value_type bonus_feat_flag {.as_string = "False", .as_int = false};
    std::vector<Attrib_value_type>  bonus_feat_levels;
    std::vector<Attrib_value_type>  bonus_feat_limits;
    std::vector<Feature_value_type> features;    // flags.
public:
    std::vector<Class_skills_information> skills;
};

///
/// Vector of class information.
/// Call with [] to get a class.
/// Use \ref get_class_list to have a vector of classes.
/// use \ref get_class_data for a vector of all \ref Class_information
///
class Class_standard {
public:
    explicit Class_standard();

    Class_information& operator[](const QString&);

    std::vector<QString> get_class_list()
    {
        return class_list;
    }

    std::vector<Class_information> get_class_data()
    {
        return class_data;
    }

private:
    std::vector<Class_information> class_data;    ///< Vector of all class data.
    std::vector<QString>           class_list;    ///< Each class for selectors
};
}    // namespace mydnd_character
#endif    // MYDND_SRC_CLASS_H

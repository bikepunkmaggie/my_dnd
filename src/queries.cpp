
#include "queries.h"

#include "../gui/error_and_debug.h"

namespace my_dnd_queries {

void error_out(const QString& message)
{
    mydnd_err::error(message.toStdString());
}
void query_prepare(QSqlQuery& query, const QString& query_string)
{
    if (!query.prepare(query_string)) {
        const QString le {"Prepare failed: " % query_string %
                          query.lastError().text()};
        mydnd_err::error(le.toStdString());
    }
}

/**
 * Execute query with error
 *
 * @exception query fails to execute.
 *
 * @param query
 */
void query_execute(QSqlQuery& query)
{
    if (!query.exec()) {
        const QString le {"Execute failed: " % query.lastQuery() %
                          query.lastError().text()};
        mydnd_err::error(le.toStdString());
    }
}
/**
 * Error check and do next in a query.
 *
 * @param query
 *
 * @exception next() fails
 *
 * @note Do not use if next can be invalid.
 */
void query_next(QSqlQuery& query)
{
    if (!query.next()) {
        const QString le {query.lastQuery() % query.lastError().text()};
        mydnd_err::error("Query next failed: ", le.toStdString());
    }
}
}    // namespace my_dnd_queries
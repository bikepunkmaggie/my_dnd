/**
 * @file
 *
 * Class and such for monsters.
 *
 * @author Mike H Benton
 *
 * @date 2025-01-25
 */

#ifndef MYDND_GUI_MONSTERS_H
#define MYDND_GUI_MONSTERS_H

#include "db_ops.h"
#include "my_dnd_enums.h"

#include <vector>

namespace mydnd_monsters {

/**
 * @class Monster_information
 * Holds information for a monster.
 *
 */
class Monster_information {
public:
    Monster_information() {};

    /**
     * Setting for the monsters name.
     *
     * @param monster_name
     */
    void set_monster_name(const QString& monster_name)
    {
        name.set_avt(monster_name);
    }

    void set_attribute(const QVariant&, const my_dnd::Monster_attributes);

    my_dnd::Attrib_value_type get_attribute(
        const my_dnd::Monster_attributes which_to_set);

    /**
     * Getter for a monsters name.
     *
     * @return Valid monster name.
     */
    QString get_monster_name()
    {
        return name.as_string;
    }

private:
    my_dnd::Attrib_value_type name;    //!< Name from  database.
    my_dnd::Attrib_value_type size;    //!< Size from database.
    my_dnd::Attrib_value_type type;    //!< Type from database.
};

/**
 * @class Monster_standard
 *
 * Holds all monsters.
 */
class Monster_standard {
public:
    explicit Monster_standard();

    Monster_information& operator[](size_t index)
    {
        return monster_data[index];
    };

    /**
     * Return all monsters.
     *
     * @return All loaded monster information.
     */
    std::vector<Monster_information> get_monster_data()
    {
        return monster_data;
    }

    const size_t monster_count()
    {
        return number_monsters;
    }

private:
    /**
     * For each monster
     */
    std::vector<Monster_information> monster_data;

    size_t number_monsters {0};
};
}    // namespace mydnd_monsters

#endif    // MYDND_GUI_MONSTERS_H

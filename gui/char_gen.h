/**
 * @file
 */
#ifndef CHAR_GEN_H
#define CHAR_GEN_H

#include "../src/character.h"
#include "../src/db_ops.h"
#include "../src/fighting_gear.h"
#include "../src/my_dnd.h"
#include "ui_char_gen.h"

#include <QDialog>
#include <QString>

namespace Ui {
class Char_gen;
}

namespace mydnd_gui {

// Helpers.
void set_dropdowns(auto member, auto* widget);
void set_dropdowns_from_attribute(const std::vector<my_dnd::Attrib_value_type>&,
                                  QComboBox*);

/**
 * A QDialog to create a new character.
 */
class Char_gen final : public QDialog {
    Q_OBJECT

public:
    explicit Char_gen(QUuid, QWidget* parent = nullptr);

    void pop_rolled();
    void pop_ability_select_groupbox();
    void set_reroll();
    void pop_general_char_selectors();
    void update_selected_ability(int, int);
    void reset();
    void update_character();
    void set_update_deity_list();
    void set_update_class_features();

    void update_abilities();
    void check_rolled();

    void do_reroll();
    void closeEvent(QCloseEvent* event);
    void set_char_complete_widget();

    bool get_char_complete()
    {
        return rca_complete;
    }

    mydnd_character::Character_information get_new_character()
    {
        return new_character;
    }
    ~Char_gen();

public slots:
private slots:
    void on_selector_race_activated(int);
    void on_selector_class_activated(int);
    void on_selector_alignment_activated(int);
    void on_str_select_activated(int);
    void on_dex_select_activated(int);
    void on_con_select_activated(int);
    void on_itl_select_activated(int);
    void on_cha_select_activated(int);
    void on_wis_select_activated(int);
    void on_reroll_button_clicked();
    void on_reset_button_clicked();
    void on_show_char_sheet_clicked();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_lock_button_clicked();
    void on_selector_sex_activated(int);
    void on_save_progress_button_clicked();

    void on_selector_deity_activated(int index);

    void on_selector_deity_textActivated(const QString& arg1);

signals:
    void char_updated(const mydnd_character::Character_information&);
    void show_hide_char_sheet();

private:
    // ui
    Ui::Char_gen* ui;

    /// bools for completions
    bool rca_complete {false};    ///< tracker for when  race/class/abilities done
    bool all_ability_selected {false};
    bool race_selected {false};
    bool class_selected {false};
    bool deity_selected {false};

    // What we are doing.
    mydnd_character::Character_information new_character;

    // Needed standards and such
    mydnd_character::Race_standard                         race_std;
    mydnd_character::Class_standard                        class_std;
    mydnd_character::Character_misc_standard               misc_info;
    mydnd_combat_gear::Weapon_standard                     weaps_std;
    std::vector<mydnd_character::Features_all>             feature_data;
    std::vector<mydnd_character::Class_skills_information> skills_info;

    /**
     * The rolled abilities.  Random filled and sorted low to high.
     */
    mydnd_character::Rolled_abilities rand_sorted;
    mydnd_character::Ability_type     indexes;

    QString            ability_pre {"Roll"};
    QString            bonus_spell_pre {"L1 Bonus Spell: "};
    QString            skill_points_pre {"Skill Points: "};
    QString            feat_points_pre {"Feats: "};
    [[nodiscard]] auto get_ability_from_list(my_dnd::ability_score_type) const;
    int                level_zero {0};
    QString            q_zero {"0"};
    void               pop_race_adjustments();
};

}    // namespace mydnd_gui

#endif    // CHAR_GEN_H

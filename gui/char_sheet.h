#ifndef CHAR_SHEET_H
#define CHAR_SHEET_H

#include "../src/character.h"
#include "../src/my_dnd.h"

#include <QDialog>
#include <QLabel>
#include <optional>
#include <qcombobox.h>

namespace Ui {
class Char_sheet;
}

namespace mydnd_gui {

QString get_attrib(const mydnd_character::Character_information& char_in,
                   const auto&                                   which_attrib)
{
    return char_in.get_attribute(which_attrib).as_string;
}

class Char_sheet;

void fill_abilities(Char_sheet*, Ui::Char_sheet*,
                    const mydnd_character::Character_information&);

/**
 * Character sheet data.
 */
class Char_sheet final : public QDialog {
    Q_OBJECT

public:
    // explicit Char_sheet(QWidget* parent = nullptr);
    Char_sheet(QWidget* parent, mydnd_character::Race_standard&,
               mydnd_character::Class_standard&);

    ~Char_sheet() final;

    void update_ability(QLabel*, QString);

    void set_tops(const mydnd_character::Character_information&);

    void set_armor_class(const my_dnd::Armor_class_data&);

public slots:
    void populate_all(const mydnd_character::Character_information&);
    void show_hide();

private:
    Ui::Char_sheet*                 ui;
    mydnd_character::Class_standard class_standard;
    mydnd_character::Race_standard  race_standard;
    QPixmap                         char_picture;
};
}    // namespace mydnd_gui

#endif    // CHAR_SHEET_H

#ifndef CHAR_CLASS_SKILLS_H
#define CHAR_CLASS_SKILLS_H

#include "ui_char_class_skills.h"

#include <QDialog>

namespace Ui {
class Char_class_skills;
}

namespace mydnd_gui {

class Char_class_skills : public QDialog {
    Q_OBJECT

public:
    explicit Char_class_skills(QWidget* parent = nullptr);
    ~        Char_class_skills() override;

public slots:
    void show_hide();

private:
    Ui::Char_class_skills* ui;
};
}    // namespace mydnd_gui
#endif    // CHAR_CLASS_SKILLS_H

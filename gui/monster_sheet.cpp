//
// Created by mbenton on 1/26/25.
//

// You may need to build the project (run Qt uic code generator) to get
// "ui_monster_sheet.h" resolved

#include "monster_sheet.h"

#include "ui_monster_sheet.h"

namespace mydnd_gui {
std::size_t next_monster(mydnd_monsters::Monster_standard& data_in, std::size_t index)
{
    size_t t = data_in.monster_count();
    ++index;
    if (index > (t - 1)) {
        --index;
    }
    return index;
}
std::size_t previous_monster(mydnd_monsters::Monster_standard& data_in,
                             std::size_t                       index)
{
    if (index == 0)
        return index;
    --index;
    return index;
}
Monster_sheet::Monster_sheet(QWidget* parent):
    QDialog(parent), ui(new Ui::Monster_sheet)
{
    this->setAttribute(Qt::WA_DeleteOnClose);
    ui->setupUi(this);
    index = 0;

    ui->previous_monster->setEnabled(false);
    ui->next_monster->setEnabled(true);
    print_monster_information(index);
}
void Monster_sheet::on_next_monster_clicked()
{
    index = next_monster(monsters, index);
    qInfo() << index << ": " << monsters.monster_count() - 1;
    if (index == (monsters.monster_count() - 1)) {
        ui->next_monster->setEnabled(false);
    }
    ui->previous_monster->setEnabled(true);
    print_monster_information(index);
}
void Monster_sheet::on_previous_monster_clicked()
{
    index = previous_monster(monsters, index);
    if (index == 0)
        ui->previous_monster->setEnabled(false);
    ui->next_monster->setEnabled(true);

    print_monster_information(index);
}
void Monster_sheet::print_monster_information(std::size_t index)
{
    auto temp {monsters[index]};
    ui->monster_name->setText(temp.get_monster_name());
    ui->monster_size->setText(
        temp.get_attribute(my_dnd::Monster_attributes::monster_size).as_string);
    ui->monster_type->setText(
        temp.get_attribute(my_dnd::Monster_attributes::monster_type).as_string);
}

}    // namespace mydnd_gui

/**
 *@file
 */
#include "main_window.h"

namespace mydnd_gui {

/**
 * Set up the main window.  The main window with the QMDI Area.
 */
Main_window::Main_window(): mdiArea(new QMdiArea)
{

    // Allow use of MDI area scroll bars.
    mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    // The mdiArea is the central widget.
    setCentralWidget(mdiArea);

    create_actions();

    status = new QLabel(this);
    create_status_bar();

    read_settings();

    setWindowTitle(tr("Dungeon and Dragons"));
    setUnifiedTitleAndToolBarOnMac(true);
}

/**
 * Close the main window (and all sub windows)
 *
 * @param event
 */
void Main_window::closeEvent(QCloseEvent* event)
{
    mdiArea->closeAllSubWindows();
    if (mdiArea->currentSubWindow()) {
        event->ignore();
    }
    else {
        write_settings();
        event->accept();
    }
}
void Main_window::open_saved_character()
{
    auto* get_uuid = new mydnd_models::Get_character_uuid(true);
    auto  check    = get_uuid->exec();
    auto  uuid     = get_uuid->get_uuid();
    if (check == QDialog::Accepted && !uuid.isNull()) {
        new_character(uuid);
    }
}
void Main_window::monster_window() const
{
    auto* disp_monster = new Monster_sheet();
    mdiArea->addSubWindow(disp_monster);
    disp_monster->open();
    disp_monster->setAttribute(Qt::WA_DeleteOnClose);

    update_status_bar("Monster TEST");
}
/**
 *  New character begin.
 *
 *  @todo Here?
 */
void Main_window::new_character(QUuid uuid) const
{
    // Set up a character generation, sheet, and other support windows.
    //    auto* new_char = new Char_gen(uuid);
    auto* new_char = new Char_gen(uuid);
    mdiArea->addSubWindow(new_char);
    new_char->open();
    new_char->move(0, 0);
    auto* all_race_data  = new mydnd_character::Race_standard();
    auto* all_class_data = new mydnd_character::Class_standard();
    new_char->setAttribute(Qt::WA_DeleteOnClose);

    auto* char_sheet = new Char_sheet(new_char, *all_race_data, *all_class_data);
    mdiArea->addSubWindow(char_sheet);
    char_sheet->setVisible(true);
    //    char_sheet->move(800, 0);
    //    char_sheet->setModal(false);

    auto* class_skills = new Char_class_skills(new_char);
    mdiArea->addSubWindow(class_skills);
    class_skills->setVisible(true);
    //    class_skills->move(800, 675);
    //    class_skills->setModal(false);

    // ALl done, get connections and update the status bar.
    const auto c_i = connect(
        new_char, SIGNAL(char_updated(mydnd_character::Character_information)),
        char_sheet, SLOT(populate_all(mydnd_character::Character_information)));
    const auto c_s  = connect(new_char, SIGNAL(show_hide_char_sheet()), char_sheet,
                              SLOT(show_hide()));
    const auto c_sk = connect(new_char, SIGNAL(show_hide_char_sheet()), class_skills,
                              SLOT(show_hide()));
    const auto c_ok =
        connect(new_char, SIGNAL(accepted()), this, SLOT(close_saved_character()));
    const auto c_cancel =
        connect(new_char, SIGNAL(rejected()), this, SLOT(close_saved_character()));
    //    const auto c_x = connect(new_char, SIGNAL(&QApplication::aboutToQuit()),
    //    this,
    //                             SLOT(close_saved_character()));
    if (!c_i || !c_s || !c_sk) {
        mydnd_err::error("No connection for new character generation.");
    }
    update_status_bar("Character Creation");

    // If we have the uuid, then update sheets, call slots.
    if (!uuid.isNull()) {
        char_sheet->populate_all(new_char->get_new_character());
    }
}

/**
 *  Stock about pop-up.
 */
void Main_window::about()
{
    QMessageBox about;
    about.setInformativeText(QCoreApplication::applicationName());
    about.setText(QCoreApplication::applicationVersion());
    about.setStandardButtons(QMessageBox::Close);
    about.exec();
}

/**
 * Menu updates
 */
void Main_window::updateWindowMenu() const
{
    windowMenu->clear();
    windowMenu->addAction(closeAct);
    windowMenu->addAction(closeAllAct);
    windowMenu->addSeparator();
    windowMenu->addAction(tileAct);
    windowMenu->addAction(cascadeAct);
    windowMenu->addSeparator();
    windowMenu->addAction(nextAct);
    windowMenu->addAction(previousAct);
    windowMenu->addAction(windowMenuSeparatorAct);

    const QList<QMdiSubWindow*> windows = mdiArea->subWindowList();
    windowMenuSeparatorAct->setVisible(!windows.isEmpty());
}

/**
 *
 *  Loads up the file menu and toolbar
 *
 *  @todo Clean up.
 */
void Main_window::create_actions()
{
    // Set the menu and toolbar objects

    // Game Menu Start
    game_menu = menuBar()->addMenu(tr("&Game"));

    // TODO: Need? QToolBar* fileToolBar = addToolBar(tr("File"));

    game_menu->addAction(tr("Switch layout direction"), this,
                         &Main_window::switch_layout_direction);

    game_menu->addSeparator();

    const QIcon exitIcon = QIcon::fromTheme("application-exit");
    QAction*    exitAct =
        game_menu->addAction(exitIcon, tr("E&xit"), qApp, &QApplication::quit);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    game_menu->addAction(exitAct);
    // Game Menu End

    // Character Menu Start
    // Use as base.  Set what in QAction and put with other info. So, this is heavy
    // comments.
    character_menu = menuBar()->addMenu(tr("&Character Menu"));

    // Action to do, set with icon and string.
    // A QAction may contain an icon, menu text, a shortcut, status text, "What's
    // This?" text, and a tool tip. Most of these can be set in the constructor. They
    // can also be set independently with setIcon(), setText(), setIconText(),
    // setShortcut(), setStatusTip(), setWhatsThis(), and setToolTip(). For menu
    // items, it is possible to set an individual font with setFont().

    // Set the icon.
    const QIcon newIcon  = QIcon(":/d20_red.jpg");
    new_character_action = new QAction(newIcon, tr("&New Character"), this);
    new_character_action->setShortcuts(QKeySequence::New);    // Ctrl+n
    new_character_action->setStatusTip(tr("Create a new Character"));
    new_character_action->setToolTip(
        tr("Start creating or resume creating a new character"));
    connect(new_character_action, &QAction::triggered, this,
            [this]() { this->new_character(QUuid()); });

    // Toolbars add
    character_toolbar = addToolBar(tr("Edit"));

    character_toolbar->addAction(new_character_action);

    // Menu completeiong for character
    character_menu->addAction(new_character_action);
    open_character_action = new QAction(tr("&Open Character"), this);
    open_character_action->setStatusTip(tr("Open saved character"));
    character_menu->addAction(open_character_action);
    character_menu->setToolTip(tr("Open a saved character."));
    connect(open_character_action, &QAction::triggered, this,
            [this]() { this->open_saved_character(); });

    // Monster Dictionary
    monster_menu             = menuBar()->addMenu(tr("&Monster Menu"));
    open_monster_dict_action = new QAction(tr("Open &Monster Dictionary"), this);
    open_monster_dict_action->setStatusTip(tr("Browse all monsters"));
    monster_menu->addAction(open_monster_dict_action);
    monster_menu->setToolTip(tr("Monster stuff"));
    connect(open_monster_dict_action, &QAction::triggered, this,
            [this]() { this->monster_window(); });

    windowMenu = menuBar()->addMenu(tr("&Window"));
    connect(windowMenu, &QMenu::aboutToShow, this, &Main_window::updateWindowMenu);

    closeAct = new QAction(tr("Cl&ose"), this);
    closeAct->setStatusTip(tr("Close the active window"));
    connect(closeAct, &QAction::triggered, mdiArea, &QMdiArea::closeActiveSubWindow);

    closeAllAct = new QAction(tr("Close &All"), this);
    closeAllAct->setStatusTip(tr("Close all the windows"));
    connect(closeAllAct, &QAction::triggered, mdiArea, &QMdiArea::closeAllSubWindows);

    tileAct = new QAction(tr("&Tile"), this);
    tileAct->setStatusTip(tr("Tile the windows"));
    connect(tileAct, &QAction::triggered, mdiArea, &QMdiArea::tileSubWindows);

    cascadeAct = new QAction(tr("&Cascade"), this);
    cascadeAct->setStatusTip(tr("Cascade the windows"));
    connect(cascadeAct, &QAction::triggered, mdiArea, &QMdiArea::cascadeSubWindows);

    nextAct = new QAction(tr("Ne&xt"), this);
    nextAct->setShortcuts(QKeySequence::NextChild);
    nextAct->setStatusTip(tr("Move the focus to the next window"));
    connect(nextAct, &QAction::triggered, mdiArea, &QMdiArea::activateNextSubWindow);

    previousAct = new QAction(tr("Pre&vious"), this);
    previousAct->setShortcuts(QKeySequence::PreviousChild);
    previousAct->setStatusTip(tr("Move the focus to the previous "
                                 "window"));
    connect(previousAct, &QAction::triggered, mdiArea,
            &QMdiArea::activatePreviousSubWindow);

    windowMenuSeparatorAct = new QAction(this);
    windowMenuSeparatorAct->setSeparator(true);

    updateWindowMenu();

    menuBar()->addSeparator();

    QMenu* helpMenu = menuBar()->addMenu(tr("&Help"));

    QAction* aboutAct = helpMenu->addAction(tr("&About"), this, &Main_window::about);
    aboutAct->setStatusTip(tr("Show the application's About box"));

    QAction* aboutQtAct =
        helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
}

/**
 * Creates and sets initial value for permanent status bar
 */
void Main_window::create_status_bar() const
{
    statusBar()->addPermanentWidget(status);
    status->clear();
    status->setText("No Character Loaded.");
}

/**
 * Updates the status bar with new_status.
 *
 * @param new_status
 */
void Main_window::update_status_bar(const QString& new_status) const
{
    status->clear();
    status->setText(new_status);
}

/**
 * Boiler plate read settings.
 *
 * @todo Check all settings
 */
void Main_window::read_settings()
{
    const QSettings  settings(QCoreApplication::organizationName(),
                              QCoreApplication::applicationName());
    const QByteArray geometry =
        settings.value("geometry", QByteArray()).toByteArray();
    if (geometry.isEmpty()) {
        const QRect availableGeometry = screen()->availableGeometry();
        resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
        move((availableGeometry.width() - width()) / 2,
             (availableGeometry.height() - height()) / 2);
    }
    else {
        restoreGeometry(geometry);
    }
}

/**
 * Boiler plate write settings.
 */
void Main_window::write_settings() const
{
    QSettings settings(QCoreApplication::organizationName(),
                       QCoreApplication::applicationName());
    settings.setValue("geometry", saveGeometry());
}

/**
 * Boiler plant switch layout.
 */
void Main_window::switch_layout_direction() const
{
    if (layoutDirection() == Qt::LeftToRight)
        QGuiApplication::setLayoutDirection(Qt::RightToLeft);
    else
        QGuiApplication::setLayoutDirection(Qt::LeftToRight);
}
}    // namespace mydnd_gui

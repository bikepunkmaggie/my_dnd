///
///@file gui_values.h
///@author Mike H Benton (bikepunk005@hotmail.com)
///@brief
///@version 0.1
///@date 2022-01-21
///
///@copyright Copyright (c) 2022
///
///

#ifndef MYDND_GUI_GUI_VALUES_H
#define MYDND_GUI_GUI_VALUES_H
#include <QString>

namespace mydnd_gui {

// General widgets abbreviations from char_sheet
inline const auto name_label      = QStringLiteral("Name");
inline const auto class_label     = QStringLiteral("Class");
inline const auto race_label      = QStringLiteral("Race");
inline const auto alingment_label = QStringLiteral("Alignment");
inline const auto diety_label     = QStringLiteral("Deity");
inline const auto level_label     = QStringLiteral("Level");
inline const auto size_label      = QStringLiteral("Size");
inline const auto age_label       = QStringLiteral("Age");
inline const auto gender_label    = QStringLiteral("Gender");
inline const auto height_label    = QStringLiteral("Height");
inline const auto weight_label    = QStringLiteral("Weight");

// Armor Class abbreviations
inline const auto ac_abbreviation                    = QStringLiteral("AC");
inline const auto ac_base_abbreviation               = QStringLiteral("Base");
inline const auto ac_armor_bonus_abbreviation        = QStringLiteral("Armor");
inline const auto ac_shield_bonus_abbreviation       = QStringLiteral("Shield");
inline const auto ac_dex_modifer_abbreviation        = QStringLiteral("Dex");
inline const auto ac_size_modifer_abbreviation       = QStringLiteral("Size");
inline const auto ac_natural_armor_abbreviation      = QStringLiteral("Natural");
inline const auto ac_deflection_modifer_abbreviation = QStringLiteral("Deflection");
inline const auto act_touch_abbreviation             = QStringLiteral("Touch");
inline const auto ac_flat_footed_abbreviation        = QStringLiteral("Flat_foot");
inline const auto ac_conditionals_abbreviation       = QStringLiteral("Conditionals");
}    // namespace mydnd_gui

#endif

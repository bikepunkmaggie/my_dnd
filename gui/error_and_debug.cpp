///
///@file error_and_debug.cpp
///@author Mike Benton (bikepunk005@hotmail.com)
///@brief Member and other functions for error
///@version 0.1
///@date 2022-01-05
///
///@copyright Copyright (c) 2022
///

#include "error_and_debug.h"
namespace mydnd_err {
// Nothing to see here
}    // namespace mydnd_err
//
// Created by mbenton on 1/26/25.
//

#ifndef MYDND_GUI_MONSTER_SHEET_H
#define MYDND_GUI_MONSTER_SHEET_H
#include "../src/monsters.h"

#include <QWidget>
#include <qdialog.h>

namespace Ui {
class Monster_sheet;
}

namespace mydnd_gui {

class Monster_sheet final : public QDialog {
    Q_OBJECT

public:
    explicit Monster_sheet(QWidget* parent = nullptr);
    ~Monster_sheet() final {};
private slots:
    void on_next_monster_clicked();
    void on_previous_monster_clicked();
    void print_monster_information(std::size_t index);

private:
    Ui::Monster_sheet* ui;

    mydnd_monsters::Monster_standard monsters;

    std::size_t index {0};
};
}    // namespace mydnd_gui

#endif    // MYDND_GUI_MONSTER_SHEET_H

/**
 *@file
 */

#include "models.h"

namespace mydnd_models {
/**
 * Default.
 */
DB_model::DB_model(QObject* parent): QSqlQueryModel(parent)
{
}

/**
 * Row count implementation.
 */
int DB_model::rowCount(const QModelIndex& parent) const
{
    return QSqlQueryModel::rowCount(parent);
}

/**
 * Column count implementation.
 */
int DB_model::columnCount(const QModelIndex& parent) const
{
    return QSqlQueryModel::columnCount(parent);
}

/**
 * data model implementation.
 *
 * @param index
 * @param role
 *
 * @return Assigned index and role.
 *
 * @todo do something.
 */
QVariant DB_model::data(const QModelIndex& index, int role = Qt::DisplayRole) const
{
    if (!index.isValid())
        return QVariant();

    // Customize display role
    if (role == Qt::DisplayRole) {
        QVariant value = QSqlQueryModel::data(index, role);
        if (index.column() == 1) {
            return value.toString().toLower();
        }

        return value;
    }
    else if (role == Qt::ToolTipRole) {
    }

    return QSqlQueryModel::data(index, role);
}

/**
 * Read whole table.
 *
 * @param table
 */
void DB_model::set_table(const QString table)
{
    QSqlQuery query;
    if (!query.exec("SELECT * FROM " + table)) {
        qDebug() << "Query execution failed:" << query.lastError().text();
    }

    this->setQuery(std::move(query));
}

/**
 * Read table, filter on.
 */
void DB_model::set_table(const QString table, const QString filter)
{
    QSqlQuery query;
    if (!query.exec("SELECT * FROM " + table + " WHERE " + filter)) {
        qDebug() << "Query execution failed:" << query.lastError().text();
    }

    this->setQuery(std::move(query));
}

/**
 * Default selections.
 */
void Character_view::set_selection_model()
{
    selection_model = this->selectionModel();
    this->setSelectionMode(SingleSelection);
    this->setSelectionBehavior(SelectRows);
    connect(selection_model, &QItemSelectionModel::selectionChanged, this,
            &Character_view::selection_data);
}
/**
 * Slot called when the selection in the model view changes. This slot checks if
 * exactly one item is selected, and if so, it prints the value of the first column of
 * the selected item to the debug output.
 *
 * @param selected The selection that has changed.
 */
void Character_view::selection_data(const QItemSelection& selected,
                                    const QItemSelection&)
{
    auto items = selected.indexes();
    if (items.size() != 1)
        return;
    auto index   = items.constFirst();
    auto column0 = index.siblingAtColumn(0);
    auto value   = column0.data().toString();
}

/**
 * Constructor for the Model_view_controller class. This constructor creates a new
 * model, view, and selection model, and sets the model and selection model for the
 * view.
 *
 * @param new_character Set to true if loading a character that is not fully created.
 * @param parent
 */
Get_character_uuid::Get_character_uuid(bool new_character, QWidget* parent):
    QDialog(parent), model(new DB_model(this)), view(new Character_view(this)),
    new_char_filter(new_character)
{
    if (new_char_filter)
        model->set_table(active_table, "new_character = true");
    else
        model->set_table(active_table, "new_character = false");

    view->setModel(model);
    view->set_selection_model();

    // Set up button box.
    buttons = new QDialogButtonBox(this);
    view->setGeometry(0, 0, 1000, 500);
    buttons->setStandardButtons(QDialogButtonBox::StandardButton::Cancel |
                                QDialogButtonBox::StandardButton::Ok);
    buttons->setGeometry(200, 200, 200, 200);

    // Standard callback
    connect(buttons, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);

    // Allow double or single click
    connect(view, &QTableView::doubleClicked, this, &Get_character_uuid::on_select);
    connect(view, &QTableView::clicked, this, &Get_character_uuid::on_select);
}

/**
 * Set the char_uuid on selection.
 */
void Get_character_uuid::on_select(QModelIndex)
{
    QModelIndex current_index = view->currentIndex();
    if (!current_index.isValid()) {
        QMessageBox::warning(this, "Selection Error", "No character selected!");
        return;
    }

    QString rv      = model->data(model->index(current_index.row(), 0)).toString();
    this->char_uuid = QUuid::fromString(rv);
}

}    // namespace mydnd_models
#include "char_sheet.h"

#include "ui_char_sheet.h"
namespace mydnd_gui {

/**
 * New character, so all available.
 *
 * @param parent
 * @param all_races
 * @param all_classes
 */
Char_sheet::Char_sheet(QWidget* parent, mydnd_character::Race_standard& all_races,
                       mydnd_character::Class_standard& all_classes):
    QDialog(parent), ui(new Ui::Char_sheet), race_standard(all_races),
    class_standard(all_classes)
{
    this->setAttribute(Qt::WA_DeleteOnClose, true);
    ui->setupUi(this);
}

///
/// Set view of armor class data
/// \param ac_data_in is \ref my_dnd::Armor_class_data
///
void Char_sheet::set_armor_class(const my_dnd::Armor_class_data& ac_data_in)
{
    ui->ac_dex_modifer->setText(ac_data_in.dexterity_modifer.as_string);
    ui->ac_size_modifer->setText(ac_data_in.size_modifer.as_string);
    ui->ac_total->setText(ac_data_in.total_ac.as_string);
}

/// Populates all widgets and refreshes calculated widgets
void Char_sheet::populate_all(const mydnd_character::Character_information& char_in)
{
    // FIXME
    // if (!char_in.character_complete())
    //  mydnd_err::error("Invalid character to char_sheet");

    fill_abilities(this, ui, char_in);

    set_tops(char_in);

    set_armor_class(char_in.get_armor_class());

    char_picture = ":/" + char_in.get_pic();
    //    QImage t;
    //    t.load(":/dwarf_ranger_female.webp");
    //    ui->character_image->setPixmap(QPixmap::fromImage(t));
    ui->character_image->setPixmap(char_picture);
}

void Char_sheet::set_tops(const mydnd_character::Character_information& new_value)
{
    using enum my_dnd::Character_attributes;
    // Need for other attributes
    QString temp_race {get_attrib(new_value, race_char)};
    if (!temp_race.isEmpty()) {
        // mydnd_character::Race_information race_data {get_race_data(temp_race)};
        const mydnd_character::Race_information race_data =
            this->race_standard[temp_race];
        ui->size->setText(
            race_data.get_race_attribute(my_dnd::Race_attributes::creat_size_attrib)
                .as_string);
    }

    ui->name->setText(get_attrib(new_value, name_char));
    ui->rate->setText(get_attrib(new_value, class_type_char));
    ui->race->setText(temp_race);
    ui->alignment->setText(get_attrib(new_value, alignment_char));
    ui->deity->setText(get_attrib(new_value, deity_char));
    ui->level->setText(get_attrib(new_value, level_char));
    ui->age->setText(get_attrib(new_value, age_char));
    ui->sex->setText(get_attrib(new_value, gender_char));
    ui->height->setText(get_attrib(new_value, height_char));
    ui->weight->setText(get_attrib(new_value, weight_char));
}

void Char_sheet::update_ability(QLabel* in, QString value)
{
    in->setText(value);
}

Char_sheet::~Char_sheet()
{
    delete ui;
}

/// Fill the 3 ability boxes: orig, mod, and total
/// Does not do in-game temp adjust
void fill_abilities(Char_sheet* sheet, Ui::Char_sheet* ui,
                    const mydnd_character::Character_information& char_in)
{
    mydnd_character::Ability_type      temp_abl {char_in.get_abilities()};
    mydnd_character::Ability_modifiers temp_mod {char_in.get_ability_modifers()};
    using namespace my_dnd;
    // For the abilities
    sheet->update_ability(ui->strength, temp_abl[Str].as_string);
    sheet->update_ability(ui->dexterity, temp_abl[Dex].as_string);
    sheet->update_ability(ui->constitution, temp_abl[Con].as_string);
    sheet->update_ability(ui->intelligence, temp_abl[Int].as_string);
    sheet->update_ability(ui->wisdom, temp_abl[Wis].as_string);
    sheet->update_ability(ui->charisma, temp_abl[Cha].as_string);

    sheet->update_ability(ui->strength_modifier, temp_mod[Str].as_string);
    sheet->update_ability(ui->dexterity_modifier, temp_mod[Dex].as_string);
    sheet->update_ability(ui->constitution_modifier, temp_mod[Con].as_string);
    sheet->update_ability(ui->intelligence_modifier, temp_mod[Int].as_string);
    sheet->update_ability(ui->wisdom_modifier, temp_mod[Wis].as_string);
    sheet->update_ability(ui->charisma_modifier, temp_mod[Cha].as_string);
}
void Char_sheet::show_hide()
{
    if (this->isVisible())
        this->hide();
    else
        this->show();
}

}    // namespace mydnd_gui

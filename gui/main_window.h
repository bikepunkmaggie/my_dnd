/**
 *@file
 */
#ifndef MYDND_MAINWINDOW_H
#define MYDND_MAINWINDOW_H

#include "char_class_skills.h"    // IWYU pragma: keep
#include "char_gen.h"
#include "char_sheet.h"    // IWYU pragma: keep
#include "models.h"
#include "monster_sheet.h"
#include "qmenu.h"
#include "qtoolbar.h"

#include <QCloseEvent>
#include <QMainWindow>    // IWYU pragma: keep
#include <QMdiArea>       // IWYU pragma: keep
#include <QMenuBar>
#include <QMessageBox>
#include <QSettings>
#include <QStatusBar>
#include <QString>

namespace mydnd_gui {

/**
 * Main window, the entry window for the game.
 */
class Main_window final : public QMainWindow {
    Q_OBJECT

public:
    Main_window();

protected:
    void closeEvent(QCloseEvent* event) override;

private slots:
    void new_character(QUuid) const;
    void monster_window() const;
    void open_saved_character();
    void updateWindowMenu() const;
    void switch_layout_direction() const;

    static void about();
    void        close_saved_character()
    {
        this->mdiArea->closeAllSubWindows();
    }

private:
    void update_status_bar(const QString&) const;

    void create_actions();
    void create_status_bar() const;
    void read_settings();
    void write_settings() const;

    QMdiArea* mdiArea;

    QMenu* game_menu {};
    QMenu* character_menu {};
    QMenu* monster_menu {};
    QMenu* windowMenu {};

    QToolBar* character_toolbar {};

    QAction* new_character_action {};
    QAction* open_character_action {};
    QAction* open_monster_dict_action {};
    QAction* closeAct {};
    QAction* closeAllAct {};
    QAction* tileAct {};
    QAction* cascadeAct {};
    QAction* nextAct {};
    QAction* previousAct {};
    QAction* windowMenuSeparatorAct {};

    QLabel* status;
};
}    // namespace mydnd_gui

#endif

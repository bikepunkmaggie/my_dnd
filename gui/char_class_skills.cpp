#include "char_class_skills.h"

namespace mydnd_gui {

Char_class_skills::Char_class_skills(QWidget* parent):
    QDialog(parent), ui(new Ui::Char_class_skills)
{
    ui->setupUi(this);
}

void Char_class_skills::show_hide()
{
    if (this->isVisible())
        this->hide();
    else
        this->show();
}

Char_class_skills::~Char_class_skills()
{
    delete ui;
}
}    // namespace mydnd_gui

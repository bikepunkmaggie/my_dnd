#!/bin/bash
# This script takes a search term as an argument and pipes it to Assistant
search_term="$1"
echo "activateKeyword $search_term" | assistant6 -enableRemoteControl &

#include "../src/db_ops.h"
#include "models.h"

#include <QApplication>
#include <QObject>
int main(int argc, char* argv[])
{
    QApplication a(argc, argv);

    my_dnd::DB_ops db;
    db.open_db("/home/mbenton/projects/my_dnd/data/data.db");

    Get_character_uuid* mcv   = new Get_character_uuid(true);
    auto                check = mcv->exec();
    if (check == QDialog::Accepted) {
        auto uid = mcv->get_uuid();
        if (!uid.isNull())
            qDebug() << uid;
    }
    //    a.exec();
    //        return a.exec();
    return EXIT_SUCCESS;
}
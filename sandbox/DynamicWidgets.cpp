#include "DynamicWidgets.h"
#include "qboxlayout.h"
#include "qnamespace.h"
#include <QCheckBox>
#include <QLabel>

DynamicWidgets::DynamicWidgets(QWidget *parent) : QMainWindow(parent) {
  ui.setupUi(this);

  QObject::connect(ui.addWidget_button, &QPushButton::clicked, this,
                   &DynamicWidgets::onAddWidget);
}

DynamicWidgets::~DynamicWidgets() {}

void DynamicWidgets::onAddWidget() {
  onRemoveWidget();
  QVBoxLayout *layout = qobject_cast<QVBoxLayout *>(ui.widgets_frame->layout());

  for (auto i = 0; i < 10; ++i) {

    QHBoxLayout *newLayout = new QHBoxLayout(); // ui.widgets_frame);

    QString buttonText = tr("Label: ").append(QString::number(i + c));
    QLabel *button = new QLabel(buttonText, ui.widgets_frame);
    newLayout->addWidget(button);
    QString buttonText1 = tr("Label2: ").append(QString::number(i));
    QLabel *button1 = new QLabel(buttonText1, ui.widgets_frame);
    newLayout->addWidget(button1);
    QCheckBox *checkBox = new QCheckBox("Check me!", ui.widgets_frame);
    newLayout->addWidget(checkBox);
    if (i == 2) {
      checkBox->setCheckState(Qt::CheckState::Checked);
    }

    layout->insertLayout(0, newLayout);

    mButtonToLayoutMap.insert(button, newLayout);
  }
  c += 10;
}

void DynamicWidgets::onRemoveWidget() {
  if (mButtonToLayoutMap.isEmpty())
    return;
  ui.widgets_frame->setUpdatesEnabled(false);
  qDeleteAll(ui.widgets_frame->findChildren<QWidget *>(
      "", Qt::FindDirectChildrenOnly));
  ui.widgets_frame->setUpdatesEnabled(true);
  //  for (auto layout : qAsConst(mButtonToLayoutMap)) {
  //  while (layout->count() != 0) {
  //  QLayoutItem *item = layout->takeAt(0);
  // delete item->widget();
  // delete item;
  // }
  //}
  mButtonToLayoutMap.clear();
}

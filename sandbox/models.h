#ifndef MYMODEL_H
#define MYMODEL_H

#include <QApplication>
#include <QDialog>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QTableView>
#include <QUuid>

/**
 * Model for the database.
 */
class DB_model : public QSqlQueryModel {
    Q_OBJECT
public:
    explicit DB_model(QObject* parent = nullptr);

    void     set_table(const QString);
    void     set_table(const QString, const QString);
    QVariant data(const QModelIndex&, int) const override;

private:
    // Overrides
    int rowCount(const QModelIndex& = QModelIndex()) const override;
    int columnCount(const QModelIndex& = QModelIndex()) const override;
};

/**
 * For viewing a character that is in the database.
 */
class Character_view : public QTableView {
    Q_OBJECT

public:
    explicit Character_view(QWidget* parent = nullptr): QTableView(parent) {};
    void set_selection_model();

public slots:
    void selection_data(const QItemSelection&, const QItemSelection&);

private:
    QItemSelectionModel* selection_model;
};

/**
 * Allow user to select a character.
 */
class Get_character_uuid : public QDialog {
    Q_OBJECT
public:
    explicit Get_character_uuid(bool     new_character = false,
                                QWidget* parent        = nullptr);
    /**
     * Call when closed to get uuid.  Can be nullptr.
     *
     * @return A QUuid object, can be null.
     */
    QUuid get_uuid()
    {
        return char_uuid;
    }

private:
    QString           active_table = "characters";
    QDialogButtonBox* buttons;
    DB_model*         model;
    Character_view*   view;
    QUuid             char_uuid;
    bool              new_char_filter;
private slots:
    void on_select(QModelIndex);
};

#endif    // MYMODEL_H

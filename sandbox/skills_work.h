#ifndef SKILLS_WORK_H
#define SKILLS_WORK_H

#include <QDialog>

namespace Ui {
class Skills_work;
}

class Skills_work : public QDialog
{
    Q_OBJECT

public:
    explicit Skills_work(QWidget *parent = nullptr);
    ~Skills_work();

private:
    Ui::Skills_work *ui;
};

#endif // SKILLS_WORK_H
